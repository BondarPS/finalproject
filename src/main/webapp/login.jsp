<%-- 
    Document   : index1
    Created on : 22.07.2015, 11:58:13
    Author     : Pavel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

        <fmt:setLocale value="${sessionScope.locale}" /> 
        <fmt:setBundle basename="prop" var="loc" /> 
        <fmt:message bundle="${loc}" key="prop.enter" var="enter" /> 
        <fmt:message bundle="${loc}" key="prop.enter_login_password" var="enter_login_password" /> 
        <fmt:message bundle="${loc}" key="prop.enter_login" var="enter_login" /> 
        <fmt:message bundle="${loc}" key="prop.enter_password" var="enter_password" /> 
        <fmt:message bundle="${loc}" key="prop.register" var="register" /> 
        <fmt:message bundle="${loc}" key="prop.enter_button" var="enter_button" />
        <fmt:message bundle="${loc}" key="prop.choose_language" var="choose_language" />
        <fmt:message bundle="${loc}" key="prop.language_ru" var="language_ru" />
        <fmt:message bundle="${loc}" key="prop.language_en" var="language_en" />

        <title>${enter}</title>


       <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <link href="css/main.css" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
                <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <c:choose>
            <c:when test="${locale eq 'ru_RU'}">
                <div class="pull-right form-inline">
                    ${choose_language}
                    <select id="locale" class="form-control">
                        <option value="ru_RU">${language_ru}</option>
                        <option value="en_EN">${language_en}</option>
                    </select>
                </div>
            </c:when>
            <c:otherwise>
                <div class="pull-right form-inline">
                    ${choose_language}
                    <select id="locale" class="form-control">
                        <option value="en_EN">${language_en}</option>
                        <option value="ru_RU">${language_ru}</option>
                    </select>
                </div>
            </c:otherwise>
        </c:choose>
        <div class="container">
            <c:if test="${ not empty message}">
                <div class="alert alert-danger">
                    ${requestScope.message}
                </div>
            </c:if>
            <form id="form" class="form-signin" action="controller" method="post">
                <h4>${enter_login_password}</h4>
                <input id="command" type="hidden" name="command" value="login"/>
                <input type="hidden" name="page" value="login_page"/>
                <input type="hidden" name="locale"/>
                <label for="inputUsername" class="sr-only">${enter_login}</label>
                <input id="inputUsername"  type="text" name="username" class="form-control" placeholder="${enter_login}"/> 
                <label for="inputPassword" class="sr-only">${enter_password}</label>
                <input id="inputPassword" type="password" name="password" class="form-control" placeholder="${enter_password}"/><br/>
                <a href="registration.jsp">${register}</a>
                <input class="btn btn-primary btn-md btn-block" type="submit" value="${enter_button}" />

            </form>
        </div>
        <script src="js/jquery-1.11.0.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/messages-${locale}.js"></script>
        <script src="js/login.js"></script>
    </body>
</html>

