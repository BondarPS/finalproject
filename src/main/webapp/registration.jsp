<%-- 
    Document   : registration
    Created on : 12.08.2015, 17:14:21
    Author     : Pavel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%> 

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


        <fmt:setLocale value="${sessionScope.locale}" /> 
        <fmt:setBundle basename="prop" var="loc" /> 
        <fmt:message bundle="${loc}" key="prop.registration" var="registration" /> 
        <fmt:message bundle="${loc}" key="prop.fill_form_fields" var="fill_form_fields" /> 
        <fmt:message bundle="${loc}" key="prop.lastName" var="lastName" /> 
        <fmt:message bundle="${loc}" key="prop.middleName" var="middleName" /> 
        <fmt:message bundle="${loc}" key="prop.firstName" var="firstName" />
        <fmt:message bundle="${loc}" key="prop.email" var="email" /> 
        <fmt:message bundle="${loc}" key="prop.phone" var="phone" /> 
        <fmt:message bundle="${loc}" key="prop.login" var="login" /> 
        <fmt:message bundle="${loc}" key="prop.password" var="password" /> 
        <fmt:message bundle="${loc}" key="prop.repeat_password" var="repeat_password" /> 
        <fmt:message bundle="${loc}" key="prop.register" var="register" /> 
        <fmt:message bundle="${loc}" key="prop.cancel" var="cancel" /> 
        <fmt:message bundle="${loc}" key="prop.enter_lastName" var="enter_lastName" /> 
        <fmt:message bundle="${loc}" key="prop.enter_firstName" var="enter_firstName" /> 
        <fmt:message bundle="${loc}" key="prop.enter_middleName" var="enter_middleName" /> 
        <fmt:message bundle="${loc}" key="prop.enter_email" var="enter_email" /> 
        <fmt:message bundle="${loc}" key="prop.enter_phone" var="enter_phone" /> 
        <fmt:message bundle="${loc}" key="prop.enter_login" var="enter_login" /> 
        <fmt:message bundle="${loc}" key="prop.enter_password" var="enter_password" /> 
        <fmt:message bundle="${loc}" key="prop.enter_repeatPassword" var="enter_repeatPassword" /> 
        <fmt:message bundle="${loc}" key="prop.choose_language" var="choose_language" />
        <fmt:message bundle="${loc}" key="prop.language_ru" var="language_ru" />
        <fmt:message bundle="${loc}" key="prop.language_en" var="language_en" />
        <fmt:message bundle="${loc}" key="prop.author" var="author" /> 
        <title>${registration}</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/main.css" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
                <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <c:choose>
            <c:when test="${locale eq 'ru_RU'}">
                <div class="pull-right form-inline">
                    ${choose_language}
                    <select id="locale" class="form-control">
                        <option value="ru_RU">${language_ru}</option>
                        <option value="en_EN">${language_en}</option>
                    </select>
                </div>
            </c:when>
            <c:otherwise>
                <div class="pull-right form-inline">
                    ${choose_language}
                    <select id="locale" class="form-control">
                        <option value="en_EN">${language_en}</option>
                        <option value="ru_RU">${language_ru}</option>
                    </select>
                </div>
            </c:otherwise>
        </c:choose>
        <div class="container">
            <form id="form" action="controller" method="post">
                <h4 class="text-center">${fill_form_fields}</h4>
                <input id="command" type="hidden" name="command" value="register"/>
                <input type="hidden" name="locale"/>
                <input type="hidden" name="page" value="register_page"/>
                <div class="form-group" id="lastNameDiv">
                    <label for="inputLastName">${lastName}</label>
                    <input type="text" class="form-control" id="inputLastName" name="lastName" placeholder="${enter_lastName}"/>
                </div>
                <div class="form-group" id="firstNameDiv">
                    <label for="inputFirstName">${firstName}</label>
                    <input type="text" class="form-control" id="inputFirstName" name="firstName" placeholder="${enter_firstName}"/>
                </div>
                <div class="form-group" id="middleNameDiv">
                    <label for="inputMiddleName">${middleName}</label>
                    <input type="text" class="form-control" id="inputMiddleName" name="middleName" placeholder="${enter_middleName}"/>
                </div>
                <div class="form-group" id="emailDiv">
                    <label for="inputEmail">${email}</label>
                    <input type="text" class="form-control" id="inputEmail" name="email" placeholder="${enter_email}"/>
                </div>
                <div class="form-group" id="phoneDiv">
                    <label for="inputPhone">${phone}</label>
                    <input type="text" class="form-control" id="inputPhone" name="phone" placeholder="${enter_phone}"/>
                </div>
                <div class="form-group" id="usernameDiv">
                    <label for="inputUsername">${login}</label>
                    <input type="text" class="form-control" id="inputUsername" name="username" placeholder="${enter_login}"/>
                </div>
                <div class="form-group" id="passwordDiv">
                    <label for="inputPassword">${password}</label>
                    <input type="password" class="form-control" id="inputPassword" name="password" placeholder="${enter_password}"/>
                </div>
                <div class="form-group" id="repeatPasswordDiv">
                    <label for="inputRepeatPassword">${repeat_password}</label>
                    <input type="password" class="form-control" id="inputRepeatPassword" name="repeatPassword" placeholder="${enter_repeatPassword}"/>
                </div>
                <div id="buttonDiv">
                    <input class="btn btn-primary" type="button" id="regButton" value="${register}" onclick="check();"/>
                    <input class="btn btn-default" type="button" value="${cancel}" onclick="history.go(-1);"/>
                </div>
            </form>
            <c:if test="${ not empty messages}">
                <c:forEach var="messageItem" items="${messages}" varStatus="iter">
                    <div class="alert alert-warning alert-dismissible" role="alert">
                        ${messageItem}
                        <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">x</span>
                        </button>
                    </div>
                </c:forEach>
            </c:if>
        </div>
        <script src="js/jquery-1.11.0.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/messages-${locale}.js"></script>
        <script src="js/registration.js"></script>
    </body>
</html>
