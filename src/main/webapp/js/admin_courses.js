window.isCourseCorrect = false;
window.isStartDateCorrect = false;
window.isEndDateCorrect = false;
window.isEndedCorreсt = false;
window.selectedItems = 0;
window.itemsText = [];


//навигация меню
function navigate(value, currentElement)
{
    var activeElement = $(".active");
    $(activeElement).toggleClass("active");
    $(currentElement).toggleClass("active");
    document.getElementById("command").value = value;
    document.getElementById("form").submit();
}

//добавление поля для ввода данных о курсе
function addItem(tableElement)
{
    var currentCell;
    var rowElement = ($('<tr>')).appendTo($('#' + tableElement).find('tbody'));
    var columnCount = $('#' + tableElement).find('tr')[0].cells.length;
    rowElement.attr('class', 'addElement');
    rowElement.append($('<th>')).attr('class', 'addElement');
    for (var i = 0; i < columnCount - 1; i++) {
        currentCell = $('<th>').appendTo(rowElement);
        currentCell.attr('class', 'addElement');
    }
    rowElement.find('th').eq(1).text(inter.courseName);
    rowElement.find('th').eq(2).text(inter.startDate);
    rowElement.find('th').eq(3).text(inter.ednDate);
    rowElement.find('th').eq(4).text(inter.is_ended);
    rowElement = ($('<tr>')).appendTo($('#' + tableElement).find('tbody'));
    rowElement.append($('<td>')).attr('class', 'addElement');
    for (var i = 0; i < columnCount - 1; i++) {
        currentCell = $('<td>').appendTo(rowElement);
        currentCell.attr('class', 'addElement');
        currentCell.append($('<input>').attr({
            type: "text",
            class: "form-control addElement"
        }));
    }
    var saveButton = $('<input>').insertAfter('#' + tableElement);
    saveButton.attr({
        type: "button",
        value: inter.save_button,
        class: "btn btn-primary addElement",
        style: "margin: 5px;",
        onclick: "addCourse();"
    });
    var cancelButton = $('<input>').insertAfter(saveButton);
    cancelButton.attr({
        type: "button",
        value: inter.cancel_button,
        class: "btn btn-default addElement",
        onclick: "deleteNewItem(" + tableElement + ")"
    });
    $('#addButton').attr('disabled', 'disabled');
    $('input:checkbox.itemCheckbox').each(function () {
        $(this).attr('disabled', 'disabled');
    });
    rowElement.find('td').eq(1).children().attr('name', 'courseName');
    rowElement.find('td').eq(2).children().attr('name', 'startDate');
    rowElement.find('td').eq(3).children().attr('name', 'endDate');
    rowElement.find('td').eq(4).children().attr('name', 'isEnded');
}

//получение ID строки таблицы, выбранной пользователем
function getSelectedItem()
{
    var selectedId;
    $('input:checkbox.itemCheckbox').each(function () {
        if (this.checked) {
            selectedId = $(this).attr('id');
        }
        $(this).attr('disabled', 'disabled');
    });
    return selectedId;
}

//при вызове данной функции данные в строке таблицы становятся редактируемыми
function editItem(id) {
    var rowElement = $('#' + id).closest('tr');
    var columnCount = rowElement[0].cells.length;
    var currentElement;
    var tableElement;
    $('#addButton').attr("disabled", "disabled");
    $('#editButton').attr("disabled", "disabled");
    $('#showDetailInformationButton').attr("disabled", "disabled");
    for (var i = 0; i < columnCount - 1; i++) {
        currentElement = rowElement.find('td:eq(' + i + ')');
        itemsText[i] = currentElement.text();
        currentElement.text("");
        currentElement.append($('<input>').attr({
            type: "text",
            class: "form-control editElement"
        }));
        currentElement.children().val(itemsText[i]);
    }
    ($('<input>')).appendTo(rowElement).attr({
        class: "editElements",
        type: "hidden",
        name: "idCourse",
        value: id
    });
    tableElement = rowElement.closest('table');
    var cancelButton = $('<input>').insertAfter(tableElement);
    cancelButton.attr({
        type: "button",
        value: inter.cancel_button,
        class: "btn btn-default editElement",
        onclick: "cancelEdit();"
    });
    var saveButton = $('<input>').insertAfter(tableElement);
    saveButton.attr({
        type: "button",
        value: inter.save_button,
        class: "btn btn-primary editElement",
        style: "margin: 5px;",
        onclick: "editCourse();"
    });
    rowElement.find('td').eq(0).children().attr('name', 'courseName');
    rowElement.find('td').eq(1).children().attr('name', 'startDate');
    rowElement.find('td').eq(2).children().attr('name', 'endDate');
    rowElement.find('td').eq(3).children().attr('name', 'isEnded');
}

//отмена редактирования таблицы, возврат к предыдущему состоянию
function cancelEdit() {
    var selectedId;
    $('input:checkbox.itemCheckbox').each(function () {
        if (this.checked) {
            selectedId = $(this).attr('id');
        }
        $(this).removeAttr('disabled', 'disabled');
    });
    var rowElement = $('#' + selectedId).closest('tr');
    var columnCount = rowElement[0].cells.length;
    for (var i = 0; i < columnCount - 1; i++) {
        currentElement = rowElement.find('td:eq(' + i + ')');
        currentElement.text(itemsText[i]);
        $(".editElement").remove();
    }
    $('#addButton').removeAttr("disabled");
    $('#editButton').removeAttr("disabled");
    $('#showDetailInformationButton').removeAttr("disabled");
}

//отмена добавления данных возврат к предыдущему состоянию
function deleteNewItem(tableElement)
{
    $(".addElement").remove();
    $('#addButton').removeAttr("disabled");
    $('input:checkbox.itemCheckbox').each(function () {
        $(this).removeAttr('disabled');
    });
}

$('input:checkbox').change(function () {
    if ($(this).is(':checked')) {
        selectedItems = selectedItems + 1;
    }
    else {
        selectedItems = selectedItems - 1;
    }
    if (selectedItems === 1) {
        $('#addButton').attr("disabled", "disabled");
        $('#editButton').removeAttr("disabled");
        $('#showDetailInformationButton').removeAttr("disabled");
    }
    else {
        $('#addButton').removeAttr("disabled");
        $('#editButton').attr("disabled", "disabled");
        $('#showDetailInformationButton').attr("disabled", "disabled");
    }
});

//проверка значений введеных пользователем
function checkValues() {
    var tableElement = $('#coursesTable');
    var messageElement;
    var buttonElement;
    if ($('input[name=courseName]').val().match(/^[А-Яа-яA-Za-z0-9\-\+ ]+$/)) {
        isCourseCorrect = true;
    }
    else {
        isCourseCorrect = false;
        messageElement = $('<div>').insertAfter(tableElement).attr({
            class: "alert alert-warning alert-dismissible",
            role: "alert"
        });
        messageElement.text(inter.incorrect_course_name);
        buttonElement = $('<button>').appendTo(messageElement).attr({
            'type': "button",
            'class': "close",
            'data-dismiss': "alert",
            'aria-label': "Close"
        });
        $('<span>').appendTo(buttonElement).attr("aria-hidden", "true").text('x');

    }
    if ($('input[name=startDate]').val().match(/^([0-9]{4}-[0-9]{2}-[0-9]{2})$/)) {
        isStartDateCorrect = true;
    }
    else {
        isStartDateCorrect = false;
        messageElement = $('<div>').insertAfter(tableElement).attr({
            class: "alert alert-warning alert-dismissible",
            role: "alert"
        });
        messageElement.text(inter.incorrect_date);
        buttonElement = $('<button>').appendTo(messageElement).attr({
            'type': "button",
            'class': "close",
            'data-dismiss': "alert",
            'aria-label': "Close"
        });
        $('<span>').appendTo(buttonElement).attr("aria-hidden", "true").text('x');
    }
    if ($('input[name=endDate]').val().match(/^([0-9]{4}-[0-9]{2}-[0-9]{2})$/)) {
        isEndDateCorrect = true;
    }
    else {
        isEndDateCorrect = false;
        messageElement = $('<div>').insertAfter(tableElement).attr({
            class: "alert alert-warning alert-dismissible",
            role: "alert"
        });
        messageElement.text(inter.incorrect_date);
        buttonElement = $('<button>').appendTo(messageElement).attr({
            'type': "button",
            'class': "close",
            'data-dismiss': "alert",
            'aria-label': "Close"
        });
        $('<span>').appendTo(buttonElement).attr("aria-hidden", "true").text('x');
    }
    if ($('input[name=isEnded]').val().match(/(^((Д|д)а)$)|(^((Н|н)ет)$)/)) {
        isEndedCorreсt = true;
    }
    else {
        isEndedCorreсt = false;
        messageElement = $('<div>').insertAfter(tableElement).attr({
            class: "alert alert-warning alert-dismissible",
            role: "alert"
        });
        messageElement.text(inter.incorrect_is_ended);
        buttonElement = $('<button>').appendTo(messageElement).attr({
            'type': "button",
            'class': "close",
            'data-dismiss': "alert",
            'aria-label': "Close"
        });
        $('<span>').appendTo(buttonElement).attr("aria-hidden", "true").text('x');
    }
    if (isCourseCorrect === true && isStartDateCorrect === true && isEndDateCorrect === true &&
            isEndedCorreсt === true) {
        return true;
    }
    else {
        return false;
    }
}

//добавление курса передача данных на сервер
function addCourse() {
    if (checkValues()) {
        $('#command').val('add_course');
        $('#form').submit();
    }
}

//редактирование данных о курсе и передача данных на сервер
function editCourse() {
    if (checkValues()) {
        $('#command').val('edit_course');
        $('#form').submit();
    }
}

//показать студентов курса
function getDetailCourseInformation (id) {
    var tableElement = $('#' + id).closest('table');
    $('#command').val('show_detail_course_information');
     var hiddenElement  = $('<input>').insertAfter(tableElement);
    hiddenElement.attr({
        type: "hidden",
        value: id,
        name: "idCourse"
    });
    $('#form').submit();
}

function logout() {
    $('#command').val('logout');
    $('#form').submit();
}