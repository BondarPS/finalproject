/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

window.inter = {
    courseName: "Course",
    startDate: "Strart date",
    ednDate: "End date",
    is_ended: "Ended",
    firstName: "First name",
    lastName: "Last Name",
    middleName: "Middle Name",
    phone: "Phone",
    email: "email",
    login: "Login",
    password: "Password",
    save_button: "Save",
    cancel_button: "Cancel",
    incorrect_course_name: "Enter correct Course Name",
    incorrect_date: 'Enter date in YYYY-MM-DD format',
    incorrect_is_ended: 'Enter Yes or No',
    enter_password: "Enter password",
    repeat_enter_password: "Repeat password",
    incorrect_password: 'field Password should contain from 8 to 16 characters',
    incorrect_repeat_password: 'Password fields  should equal',
    incorrect_first_name: 'field First Name should contain only cyrillic or latinas characters',
    incorrect_last_name: 'field Last Name should contain only cyrillic or latinas characters',
    incorrect_middle_name: 'field Middle Name should contain only cyrillic or latinas characters',
    incorrect_email: 'Enter correct email',
    incorrect_phone: 'Enter phone in 375********* format',
    incorrect_username: 'field Login should contain only 6 latinas characters ',
    incorrect_mark: 'Enter correct mark from 0 to 10',
    incorrect_comments: 'Comments should not exceed 255 characters, and should contain punctuation, figures and letters',
    check_corect_fields: "Check fields"
    
};
