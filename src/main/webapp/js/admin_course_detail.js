
//навигация меню
function navigate(value, currentElement)
{
    var activeElement = $(".active");
    $(activeElement).toggleClass("active");
    $(currentElement).toggleClass("active");
    document.getElementById("command").value = value;
    document.getElementById("form").submit();
}

//показать студентов курса
function appointTeacher() {
    $('#command').val('appoint_teacher');
    $('#form').submit();
}

function logout() {
    $('#command').val('logout');
    $('#form').submit();
}

function back (value) {
    document.getElementById("command").value = value;
    document.getElementById("form").submit();
}