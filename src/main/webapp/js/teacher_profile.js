window.isfirstNameCorrect = false;
window.isLastNameCorrect = false;
window.isMiddleNameCorrect = false;
window.isEmailCorreсt = false;
window.isPhoneNumberCorreсt = false;
window.isPasswordCorreсt = false;
window.isRepeatPasswordCorrect = false;
window.isMarkCorrect = false;
window.isCommentsCorrect = false;
window.selectedItems = 0;
window.itemsText = [];
window.itemNames = ['firstName', 'lastName', 'middleName', 'phone', 'email'];


//навигация меню
function navigate(value, currentElement)
{
    var activeElement = $(".active");
    $(activeElement).toggleClass("active");
    $(currentElement).toggleClass("active");
    document.getElementById("command").value = value;
    document.getElementById("form").submit();
}


function chnageProfile() {
    var currentElement;
    $('#changeProfileDataButton').attr({
        disabled: "disabled",
        style: "visibility: hidden;"
    });
    $('#changePasswordButton').attr({
        disabled: "disabled",
        style: "visibility: hidden;"
    });
    for (var i = 0; i < itemNames.length; i++) {
        currentElement = $('#' + itemNames[i] + '');
        itemsText[i] = currentElement.text();
        currentElement.text("");
        currentElement.append($('<input>').attr({
            type: "text",
            class: "form-control editElement",
            name: itemNames[i]
        }));
        currentElement.children().val(itemsText[i]);
    }

    var saveButton = $('<input>').appendTo($('#studentProfile'));
    saveButton.attr({
        type: "button",
        value: inter.save_button,
        class: "btn btn-primary editElement",
        style: "margin: 5px;",
        onclick: "editProfile();"
    });

    var cancelButton = $('<input>').appendTo($('#studentProfile'));
    cancelButton.attr({
        type: "button",
        value: inter.cancel_button,
        class: "btn btn-default editElement",
        onclick: "cancelEdit();"
    });
}

//отмена редактирования, возврат к предыдущему состоянию
function cancelEdit() {
    for (var i = 0; i < itemNames.length; i++) {
        currentElement = $('#' + itemNames[i] + '');
        currentElement.text(itemsText[i]);
        $(".editElement").remove();
    }
    $('#changeProfileDataButton').removeAttr("disabled");
    $('#changeProfileDataButton').removeAttr("style");
    $('#changePasswordButton').removeAttr("disabled");
    $('#changePasswordButton').removeAttr("style");
}

//добавление интерфейса для смены пароля
function addChangePasswordFields() {
    var currentElement;
    $('#changeProfileDataButton').attr({
        disabled: "disabled",
        style: "visibility: hidden;"
    });
    $('#changePasswordButton').attr({
        disabled: "disabled",
        style: "visibility: hidden;"
    });
    var divElement = $('<div>').appendTo($('#studentProfile'));
    divElement.attr("class", "form-group changePasswordElements");
    currentElement = $('<input>').appendTo(divElement);
    currentElement.attr({
        class: "form-control changePasswordElements",
        type: "password",
        name: "password",
        placeholder: inter.enter_password
    });
    currentElement = $('<input>').appendTo(divElement);
    currentElement.attr({
        class: "form-control changePasswordElements",
        type: "password",
        name: "repeatPassword",
        placeholder: inter.repeat_enter_password
    });

    $('<br>').insertAfter(divElement);
    var cancelButton = $('<input>').insertAfter(divElement);
    cancelButton.attr({
        type: "button",
        value: inter.cancel_button,
        class: "btn btn-default changePasswordElements",
        onclick: "cancelChangePassword();"
    });
    var saveButton = $('<input>').insertAfter(divElement);
    saveButton.attr({
        type: "button",
        value: inter.save_button,
        class: "btn btn-primary changePasswordElements",
        style: "margin: 5px;",
        onclick: "changePassword();"
    });
}

function cancelChangePassword() {
    $(".changePasswordElements").remove();
    $('#changeProfileDataButton').removeAttr("disabled");
    $('#changeProfileDataButton').removeAttr("style");
    $('#changePasswordButton').removeAttr("disabled");
    $('#changePasswordButton').removeAttr("style");
}

//проверка значений введеных пользователем
function checkValues(functionName) {
    var formElement = $('#form');
    var messageElement;
    var buttonElement;
    if (functionName === 'putMark') {
        if ($('input[name=mark]').val().match(/(^([1-9]|10){1})$/)) {
            isMarkCorrect = true;
        }
        else {
            isMarkCorrect = false;
            messageElement = $('<div>').insertAfter(formElement).attr({
                class: "alert alert-warning alert-dismissible",
                role: "alert"
            });
            messageElement.text(inter.incorrect_mark);
            buttonElement = $('<button>').appendTo(messageElement).attr({
                'type': "button",
                'class': "close",
                'data-dismiss': "alert",
                'aria-label': "Close"
            });
            $('<span>').appendTo(buttonElement).attr("aria-hidden", "true").text('x');
        }
        if ($('input[name=comments]').val().match(/^[A-Za-zА-Яа-я0-9\s\?\.\!\_\-\+\,]{1,255}$/)) {
            isCommentsCorrect = true;
        }
        else {
            isCommentsCorrect = false;
            messageElement = $('<div>').insertAfter(formElement).attr({
                class: "alert alert-warning alert-dismissible",
                role: "alert"
            });
            messageElement.text(inter.incorrect_comments);
            buttonElement = $('<button>').appendTo(messageElement).attr({
                'type': "button",
                'class': "close",
                'data-dismiss': "alert",
                'aria-label': "Close"
            });
            $('<span>').appendTo(buttonElement).attr("aria-hidden", "true").text('x');
        }
        if (isMarkCorrect === true && isCommentsCorrect === true) {
            return true;
        }
        else {
            return false;
        }
    }
    if (functionName === 'password') {
        if ($('input[name=password]').val().match(/^.{8,16}$/)) {
            isPasswordCorreсt = true;
        }
        else {
            isPasswordCorreсt = false;
            messageElement = $('<div>').insertAfter(formElement).attr({
                class: "alert alert-warning alert-dismissible",
                role: "alert"
            });
            messageElement.text(inter.incorrect_password);
            buttonElement = $('<button>').appendTo(messageElement).attr({
                'type': "button",
                'class': "close",
                'data-dismiss': "alert",
                'aria-label': "Close"
            });
            $('<span>').appendTo(buttonElement).attr("aria-hidden", "true").text('x');
        }
        if ($('input[name=repeatPassword]').val() === $('input[name=password]').val()) {
            isRepeatPasswordCorreсt = true;
        }
        else {
            isRepeatPasswordCorreсt = false;
            messageElement = $('<div>').insertAfter(formElement).attr({
                class: "alert alert-warning alert-dismissible",
                role: "alert"
            });
            messageElement.text(inter.incorrect_repeat_password);
            buttonElement = $('<button>').appendTo(messageElement).attr({
                'type': "button",
                'class': "close",
                'data-dismiss': "alert",
                'aria-label': "Close"
            });
            $('<span>').appendTo(buttonElement).attr("aria-hidden", "true").text('x');
        }
        if (isPasswordCorreсt === true && isRepeatPasswordCorreсt === true) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        if ($('input[name=firstName]').val().match(/(^[А-Яа-я]+)$/)) {
            isfirstNameCorrect = true;
        }
        else {
            isfirstNameCorrect = false;
            messageElement = $('<div>').insertAfter(formElement).attr({
                class: "alert alert-warning alert-dismissible",
                role: "alert"
            });
            messageElement.text(inter.incorrect_first_name);
            buttonElement = $('<button>').appendTo(messageElement).attr({
                'type': "button",
                'class': "close",
                'data-dismiss': "alert",
                'aria-label': "Close"
            });
            $('<span>').appendTo(buttonElement).attr("aria-hidden", "true").text('x');

        }
        if ($('input[name=lastName]').val().match(/(^[А-Яа-я]+)$/)) {
            isLastNameCorrect = true;
        }
        else {
            isLastNameCorrect = false;
            messageElement = $('<div>').insertAfter(formElement).attr({
                class: "alert alert-warning alert-dismissible",
                role: "alert"
            });
            messageElement.text(inter.incorrect_last_name);
            buttonElement = $('<button>').appendTo(messageElement).attr({
                'type': "button",
                'class': "close",
                'data-dismiss': "alert",
                'aria-label': "Close"
            });
            $('<span>').appendTo(buttonElement).attr("aria-hidden", "true").text('x');

        }
        if ($('input[name=middleName]').val().match(/(^[А-Яа-я]+)$/)) {
            isMiddleNameCorrect = true;
        }
        else {
            isMiddleNameCorrect = false;
            messageElement = $('<div>').insertAfter(formElement).attr({
                class: "alert alert-warning alert-dismissible",
                role: "alert"
            });
            messageElement.text(inter.incorrect_middle_name);
            buttonElement = $('<button>').appendTo(messageElement).attr({
                'type': "button",
                'class': "close",
                'data-dismiss': "alert",
                'aria-label': "Close"
            });
            $('<span>').appendTo(buttonElement).attr("aria-hidden", "true").text('x');

        }

        if ($('input[name=email]').val().match(/^[A-Za-z/_0-9/./-]*[@][A-Za-z/_0-9/-]*[.][A-Za-z]{2,3}$/)) {
            isEmailCorreсt = true;
        }
        else {
            isEmailCorreсt = false;
            messageElement = $('<div>').insertAfter(formElement).attr({
                class: "alert alert-warning alert-dismissible",
                role: "alert"
            });
            messageElement.text(inter.incorrect_email);
            buttonElement = $('<button>').appendTo(messageElement).attr({
                'type': "button",
                'class': "close",
                'data-dismiss': "alert",
                'aria-label': "Close"
            });
            $('<span>').appendTo(buttonElement).attr("aria-hidden", "true").text('x');
        }
        if ($('input[name=phone]').val().match(/^(375)+[0-9]{9}$/)) {
            isPhoneNumberCorreсt = true;
        }
        else {
            isPhoneNumberCorreсt = false;
            messageElement = $('<div>').insertAfter(formElement).attr({
                class: "alert alert-warning alert-dismissible",
                role: "alert"
            });
            messageElement.text(inter.incorrect_phone);
            buttonElement = $('<button>').appendTo(messageElement).attr({
                'type': "button",
                'class': "close",
                'data-dismiss': "alert",
                'aria-label': "Close"
            });
            $('<span>').appendTo(buttonElement).attr("aria-hidden", "true").text('x');
        }
        if (isfirstNameCorrect === true && isLastNameCorrect === true && isMiddleNameCorrect === true &&
                isEmailCorreсt === true && isPhoneNumberCorreсt === true) {
            return true;
        }
        else {
            return false;
        }
    }
}

//редактирование данных о студенте и передача данных на сервер
function editProfile() {
    if (checkValues('edit')) {
        $('#command').val('edit_student');
        $('#form').submit();
    }
}

//показать студентов курса
function showCourseStudent(id) {
    var tableElement = $('#' + id).closest('table');
    $('#command').val('show_course_students');
    var hiddenElement = $('<input>').insertAfter(tableElement);
    hiddenElement.attr({
        type: "hidden",
        value: id,
        name: "idCourse"
    });
    $('#form').submit();
}


// функция смены пароля
function changePassword() {
    if (checkValues('password')) {
        $('#command').val('change_password');
        $('#form').submit();
    }
}

//выставить отметку
function addEditMarkFields(clickedButton) {
    var currentElement;
    var idButton = clickedButton.id;
    ($('<input>')).insertAfter(clickedButton).attr({
        class: "editElements",
        type: "hidden",
        name: "idMark",
        value: idButton
    });
    
    var rowElement = $('#' + idButton).closest('tr');
    var columnCount = rowElement[0].cells.length;
    
    currentElement = rowElement.find('td:eq(' + (columnCount - 2) + ')');
    itemsText[0] = currentElement.text();
    currentElement.text("");
    currentElement.append($('<input>').attr({
        type: "text",
        class: "form-control editElement",
        name: "comments"
    }));
    currentElement.children().val(itemsText[0]);
    currentElement = rowElement.find('td:eq(' + (columnCount - 3) + ')');
    itemsText[1] = currentElement.text();
    currentElement.text("");
    currentElement.append($('<input>').attr({
        type: "text",
        class: "form-control editElement",
        name: "mark"
    }));
    currentElement.children().val(itemsText[1]);
    currentElement = $('<td>').appendTo(rowElement);
    currentElement.attr("class", "editElement");
    var saveButton = $('<input>').appendTo(currentElement);
    saveButton.attr({
        type: "button",
        value: inter.save_button,
        class: "btn btn-primary editElement",
        style: "margin: 5px;",
        onclick: "putMark();"
    });
    currentElement = $('<td>').appendTo(rowElement);
    currentElement.attr("class", "editElement");
    var cancelButton = $('<input>').appendTo(currentElement);
    cancelButton.attr({
        type: "button",
        value: inter.cancel_button,
        class: "btn btn-default editElement",
        onclick: "cancelPutMark("+idButton+");"
    });
    $('.putMarkButton').attr("disabled", "disabled");
}

function putMark() {
    if (checkValues('putMark')) {
        $('#command').val('put_mark');
        $('#form').submit();
    }
}

function cancelPutMark(id) {
    $('.editElement').remove();
    var rowElement = $('#' + id).closest('tr');
    var columnCount = rowElement[0].cells.length;
    currentElement = rowElement.find('td:eq(' + (columnCount - 2) + ')');
    currentElement.text(itemsText[0]);
    currentElement = rowElement.find('td:eq(' + (columnCount - 3) + ')');
    currentElement.text(itemsText[1]);
}


$('input:checkbox').change(function () {
    if ($(this).is(':checked')) {
        selectedItems = selectedItems + 1;
    }
    else {
        selectedItems = selectedItems - 1;
    }
    if (selectedItems === 1) {
        $('#showStudentButton').removeAttr("disabled", "disabled");
    }
    else {
        $('#showStudentButton').attr("disabled", "disabled");
    }
});

//получение ID строки таблицы, выбранной пользователем
function getSelectedItem()
{
    var selectedId;
    $('input:checkbox.itemCheckbox').each(function () {
        if (this.checked) {
            selectedId = $(this).attr('id');
        }
        $(this).attr('disabled', 'disabled');
    });
    return selectedId;
}


function logout() {
    $('#command').val('logout');
    $('#form').submit();
}

function back (value) {
    document.getElementById("command").value = value;
    document.getElementById("form").submit();
}