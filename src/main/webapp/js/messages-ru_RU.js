/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

window.inter = {
    courseName: "Курс",
    startDate: "Период с",
    ednDate: "Период по",
    is_ended: "Окончен",
    firstName: "Имя",
    lastName: "Фамилия",
    middleName: "Отчество",
    phone: "Телефон",
    email: "email",
    login: "Логин",
    password: "Пароль",
    save_button: "Сохранить",
    cancel_button: "Отмена",
    incorrect_course_name: "Введите корректное название курса",
    incorrect_date: 'Введите дату в формате ГГГГ-ММ-ДД',
    incorrect_is_ended: 'Введите Да или Нет в поле Окончен',
    enter_password: "Введите пароль",
    repeat_enter_password: "Повторите пароль",
    incorrect_password: 'Поле пароль должно содержать минимум 8 символов',
    incorrect_repeat_password: 'Пароли должны совпадать',
    incorrect_first_name: 'Введите корректное имя',
    incorrect_last_name: 'Введите корректную фамилию',
    incorrect_middle_name: 'Введите корректное отчество',
    incorrect_email: 'Введите корректный email',
    incorrect_phone: 'Укажите телефон в следующем формате 375*********',
    incorrect_username: 'Введите корреткное имя пользователя, поле должно содержать не менее 6 символов',
    incorrect_mark: 'Введите корректную оценку от 0 до 10',
    incorrect_comments: 'Комментарий не должен превышать 255 символов, и содержать только знаки препинания, цифры и буквенные символы',
    check_corect_fields: "Проверьте корректность заполнения полей"
    
};
