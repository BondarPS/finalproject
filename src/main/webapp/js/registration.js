window.firstNameAlert = null;
window.lastNameAlert = null;
window.middleNameAlert = null;
window.phoneAlert = null;
window.emailAlert = null;
window.passwordAlert = null;
window.repeatPasswordAlert = null;
window.usernameAlert = null;
window.buttonAlert = null;


function check() {
    var isFirstNameCorrect = false;
    var isLastNameCorrect = false;
    var isMiddleNameCorrect = false;
    var isPhoneCorrect = false;
    var isEmailCorrect = false;
    var isUsernameCorrect = false;
    var isPasswordCorrect = false;
    var isRepeatPasswordCorrect = false;
    var result = false;

    if (document.getElementById('inputFirstName').value.match(/(^([А-Яа-я]+)$)|(^([A-Za-z]+)$)/)) {
        isFirstNameCorrect = true;
        if (firstNameAlert !== null) {
            document.getElementById('inputFirstName').parentNode.removeChild(firstNameAlert);
            $('#firstNameDiv').toggleClass('has-error');
            firstNameAlert = null;
        }
    }
    else {
        isFirstNameCorrect = false;
        if (firstNameAlert === null) {
            firstNameAlert = document.createElement('label');
            firstNameAlert.innerHTML = inter.incorrect_first_name;
            $(firstNameAlert).toggleClass('control-label');
            document.getElementById('inputFirstName').parentNode.insertBefore(firstNameAlert, this.nextSibling);
            $('#firstNameDiv').toggleClass('has-error');
        }
    }
    if (document.getElementById('inputLastName').value.match(/(^([А-Яа-я]+)$)|(^([A-Za-z]+)$)/)) {
        isLastNameCorrect = true;
        if (lastNameAlert !== null) {
            document.getElementById('inputLastName').parentNode.removeChild(lastNameAlert);
            $('#lastNameDiv').toggleClass('has-error');
            lastNameAlert = null;
        }
    }
    else {
        isLastNameCorrect = false;
        if (lastNameAlert === null) {
            lastNameAlert = document.createElement('label');
            lastNameAlert.innerHTML = inter.incorrect_last_name;
            $(lastNameAlert).toggleClass('control-label');
            document.getElementById('inputLastName').parentNode.insertBefore(lastNameAlert, this.nextSibling);
            $('#lastNameDiv').toggleClass('has-error');
        }
    }
    if (document.getElementById('inputMiddleName').value.match(/(^([А-Яа-я]+)$)|(^([A-Za-z]+)$)/)) {
        isMiddleNameCorrect = true;
        if (middleNameAlert !== null) {
            document.getElementById('inputMiddleName').parentNode.removeChild(middleNameAlert);
            $('#middleNameDiv').toggleClass('has-error');
            middleNameAlert = null;
        }
    }
    else {
        isMiddleNameCorrect = false;
        if (middleNameAlert === null) {
            middleNameAlert = document.createElement('label');
            middleNameAlert.innerHTML = inter.incorrect_middle_name;
            $(middleNameAlert).toggleClass('control-label');
            document.getElementById('inputMiddleName').parentNode.insertBefore(middleNameAlert, this.nextSibling);
            $('#middleNameDiv').toggleClass('has-error');
        }
    }
    if (document.getElementById('inputPhone').value.match(/^(375)+[0-9]{9}$/)) {
        isPhoneCorrect = true;
        if (phoneAlert !== null) {
            document.getElementById('inputPhone').parentNode.removeChild(phoneAlert);
            $('#phoneDiv').toggleClass('has-error');
            phoneAlert = null;
        }
    }
    else {
        isPhoneCorrect = false;
        if (phoneAlert === null) {
            phoneAlert = document.createElement('label');
            phoneAlert.innerHTML = inter.incorrect_phone;
            $(phoneAlert).toggleClass('control-label');
            document.getElementById('inputPhone').parentNode.insertBefore(phoneAlert, this.nextSibling);
            $('#phoneDiv').toggleClass('has-error');
        }
    }
    if (document.getElementById('inputEmail').value.match(/^[A-Za-z/_0-9/./-]*[@][A-Za-z/_0-9/-]*[.][A-Za-z]{2,3}$/)) {
        isEmailCorrect = true;
        if (emailAlert !== null) {
            document.getElementById('inputEmail').parentNode.removeChild(emailAlert);
            $('#emailDiv').toggleClass('has-error');
            emailAlert = null;
        }
        ;
    }
    else {
        isEmailCorrect = false;
        if (emailAlert === null) {
            emailAlert = document.createElement('label');
            emailAlert.innerHTML = inter.incorrect_email;
            $(emailAlert).toggleClass('control-label');
            document.getElementById('inputEmail').parentNode.insertBefore(emailAlert, this.nextSibling);
            $('#emailDiv').toggleClass('has-error');
        }
    }
    if (document.getElementById('inputUsername').value.match(/(^([A-Za-z0-9\-\_]{6,})$)/)) {
        isUsernameCorrect = true;
        if (usernameAlert !== null) {
            document.getElementById('inputUsername').parentNode.removeChild(usernameAlert);
            $('#usernameDiv').toggleClass('has-error');
            usernameAlert = null;
        }
    }
    else {
        isUsernameCorrect = false;
        if (usernameAlert === null) {
            usernameAlert = document.createElement('label');
            usernameAlert.innerHTML = inter.incorrect_username;
            $(usernameAlert).toggleClass('control-label');
            document.getElementById('inputUsername').parentNode.insertBefore(usernameAlert, this.nextSibling);
            $('#usernameDiv').toggleClass('has-error');
        }
    }
    if (document.getElementById('inputPassword').value.match(/^.{8,16}$/)) {
        isPasswordCorrect = true;
        if (passwordAlert !== null) {
            document.getElementById('inputPassword').parentNode.removeChild(passwordAlert);
            $('#passwordDiv').toggleClass('has-error');
            passwordAlert = null;
        }
    }
    else {
        isPasswordCorrect = false;
        if (passwordAlert === null) {
            passwordAlert = document.createElement('label');
            passwordAlert.innerHTML = inter.incorrect_password;
            $(passwordAlert).toggleClass('control-label');
            document.getElementById('inputPassword').parentNode.insertBefore(passwordAlert, this.nextSibling);
            $('#passwordDiv').toggleClass('has-error');
        }
    }
    if (document.getElementById('inputRepeatPassword').value === document.getElementById('inputPassword').value) {
        isRepeatPasswordCorrect = true;
        if (repeatPasswordAlert !== null) {
            document.getElementById('inputRepeatPassword').parentNode.removeChild(repeatPasswordAlert);
            $('#repeatPasswordDiv').toggleClass('has-error');
            repeatPasswordAlert = null;
        }
    }
    else {
        isRepeatPasswordCorrect = false;
        if (repeatPasswordAlert === null) {
            repeatPasswordAlert = document.createElement('label');
            repeatPasswordAlert.innerHTML = inter.incorrect_repeat_password;
            $(repeatPasswordAlert).toggleClass('control-label');
            document.getElementById('inputRepeatPassword').parentNode.insertBefore(repeatPasswordAlert, this.nextSibling);
            $('#repeatPasswordDiv').toggleClass('has-error');
        }
    }
    if (isPasswordCorrect === true && isRepeatPasswordCorrect === true && isEmailCorrect === true && isPhoneCorrect === true
            && isFirstNameCorrect === true && isLastNameCorrect === true && isMiddleNameCorrect === true) {
        if (buttonAlert !== null) {
            document.getElementById('regButton').parentNode.removeChild(buttonAlert);
            $('#buttonDiv').toggleClass('has-error');
            buttonAlert = null;
        }
        result = true;
        window.form.submit();
    }
    else {
        if (buttonAlert === null) {
            buttonAlert = document.createElement('label');
            buttonAlert.innerHTML = inter.check_corect_fields;
            $(buttonAlert).toggleClass('control-label');
            document.getElementById('regButton').parentNode.insertBefore(buttonAlert, this.nextSibling);
            $('#buttonDiv').toggleClass('has-error');
        }
    }
    return result;
}
;

$("#locale").change(function () {
    $('#command').val('change_locale');
    $('input[name=locale]').val($('#locale').val());
    $('#form').submit();
});