window.isfirstNameCorrect = false;
window.isLastNameCorrect = false;
window.isMiddleNameCorrect = false;
window.isEmailCorreсt = false;
window.isPhoneNumberCorreсt = false;
window.isUsernameCorreсt = false;
window.isPasswordCorreсt = false;
window.isRepeatPasswordCorrect = false;
window.selectedItems = 0;
window.itemsText = [];

//навигация меню
function navigate(value, currentElement)
{
    var activeElement = $(".active");
    $(activeElement).toggleClass("active");
    $(currentElement).toggleClass("active");
    document.getElementById("command").value = value;
    document.getElementById("form").submit();
}

//получение ID строки таблицы, выбранной пользователем
function getSelectedItem()
{
    var selectedId;
    $('input:checkbox.itemCheckbox').each(function() {
        if (this.checked) {
            selectedId = $(this).attr('id');
        }
        $(this).attr('disabled', 'disabled');
    });
    return selectedId;
}


//при вызове данной функции данные в строке таблицы становятся редактируемыми
function editItem(id) {
    var rowElement = $('#' + id).closest('tr');
    var columnCount = rowElement[0].cells.length;
    var currentElement;
    var tableElement;
    $('#editButton').attr("disabled", "disabled");
    $('#deleteButton').attr("disabled", "disabled");
    $('#showMarksButton').attr("disabled", "disabled");
    $('#showCoursesButton').attr("disabled", "disabled");
    $('#changePasswordButton').attr("disabled", "disabled");
    for (var i = 0; i < columnCount - 1; i++) {
        currentElement = rowElement.find('td:eq(' + i + ')');
        itemsText[i] = currentElement.text();
        currentElement.text("");
        currentElement.append($('<input>').attr({
            type: "text",
            class: "form-control editElement"
        }));
        currentElement.children().val(itemsText[i]);
    }
    $('<input>').appendTo(rowElement).attr({
        class: "editElements",
        type: "hidden",
        name: "idStudent",
        value: id
    });
    tableElement = rowElement.closest('table');
    var cancelButton = $('<input>').insertAfter(tableElement);
    cancelButton.attr({
        type: "button",
        value: inter.cancel_button,
        class: "btn btn-default editElement",
        onclick: "cancelEdit();"
    });
    var saveButton = $('<input>').insertAfter(tableElement);
    saveButton.attr({
        type: "button",
        value: inter.save_button,
        class: "btn btn-primary editElement",
        style: "margin: 5px;",
        onclick: "editStudent();"
    });
    rowElement.find('td').eq(0).children().attr('name', 'firstName');
    rowElement.find('td').eq(1).children().attr('name', 'lastName');
    rowElement.find('td').eq(2).children().attr('name', 'middleName');
    rowElement.find('td').eq(3).children().attr('name', 'email');
    rowElement.find('td').eq(4).children().attr('name', 'phone');
    rowElement.find('td').eq(5).children().attr('name', 'username');
}

//отмена редактирования таблицы, возврат к предыдущему состоянию
function cancelEdit() {
    var selectedId;
    $('input:checkbox.itemCheckbox').each(function() {
        if (this.checked) {
            selectedId = $(this).attr('id');
        }
        $(this).removeAttr('disabled', 'disabled');
    });
    var rowElement = $('#' + selectedId).closest('tr');
    var columnCount = rowElement[0].cells.length;
    for (var i = 0; i < columnCount - 1; i++) {
        currentElement = rowElement.find('td:eq(' + i + ')');
        currentElement.text(itemsText[i]);
        $(".editElement").remove();
    }
    $('#editButton').attr("disabled", "disabled");
    $('#deleteButton').attr("disabled", "disabled");
    $('#showMarksButton').attr("disabled", "disabled");
    $('#showCoursesButton').attr("disabled", "disabled");
    $('#changePasswordButton').attr("disabled", "disabled");
}

//добавление интерфейса для смены пароля
function addChangePasswordFields(id) {
    var rowElement = $('#' + id).closest('tr');
    var currentElement;
    var tableElement;
    $('#editButton').attr("disabled", "disabled");
    $('#deleteButton').attr("disabled", "disabled");
    $('#showMarksButton').attr("disabled", "disabled");
    $('#showCoursesButton').attr("disabled", "disabled");
    $('#changePasswordButton').attr("disabled", "disabled");

    tableElement = rowElement.closest('table');
    currentElement = $('<input>').insertAfter(tableElement);
    currentElement.attr({
        class: "changePasswordElements",
        type: "hidden",
        name: "idStudent",
        value: id
    });
    var divElement = $('<div>').insertAfter(tableElement);
    divElement.attr("class", "form-group changePasswordElements");
    currentElement = $('<input>').appendTo(divElement);
    currentElement.attr({
        class: "form-control changePasswordElements",
        type: "password",
        name: "password",
        placeholder: inter.enter_password
    });
    currentElement = $('<input>').appendTo(divElement);
    currentElement.attr({
        class: "form-control changePasswordElements",
        type: "password",
        name: "repeatPassword",
        placeholder: inter.repeat_enter_password
    });

    $('<br>').insertAfter(divElement);
    var cancelButton = $('<input>').insertAfter(divElement);
    cancelButton.attr({
        type: "button",
        value: inter.cancel_button,
        class: "btn btn-default changePasswordElements",
        onclick: "cancelChangePassword();"
    });
    var saveButton = $('<input>').insertAfter(divElement);
    saveButton.attr({
        type: "button",
        value: inter.save_button,
        class: "btn btn-primary changePasswordElements",
        style: "margin: 5px;",
        onclick: "changePassword();"
    });
}

function cancelChangePassword() {
    var selectedId;
    $('input:checkbox.itemCheckbox').each(function() {
        if (this.checked) {
            selectedId = $(this).attr('id');
        }
        $(this).removeAttr('disabled', 'disabled');
    });
    $(".changePasswordElements").remove();
    $('#editButton').attr("disabled", "disabled");
    $('#deleteButton').attr("disabled", "disabled");
    $('#showMarksButton').attr("disabled", "disabled");
    $('#showCoursesButton').attr("disabled", "disabled");
    $('#changePasswordButton').attr("disabled", "disabled");
}


//проверка значений введеных пользователем
function checkValues(functionName) {
    var tableElement = $('#studentsTable');
    var messageElement;
    var buttonElement;
    if (functionName === 'password') {
        if ($('input[name=password]').val().match(/^.{8,16}$/)) {
            isPasswordCorreсt = true;
        }
        else {
            isPasswordCorreсt = false;
            messageElement = $('<div>').insertAfter(tableElement).attr({
                class: "alert alert-warning alert-dismissible",
                role: "alert"
            });
            messageElement.text(inter.incorrect_password);
            buttonElement = $('<button>').appendTo(messageElement).attr({
                'type': "button",
                'class': "close",
                'data-dismiss': "alert",
                'aria-label': "Close"
            });
            $('<span>').appendTo(buttonElement).attr("aria-hidden", "true").text('x');
        }
        if ($('input[name=repeatPassword]').val() === $('input[name=password]').val()) {
            isRepeatPasswordCorreсt = true;
        }
        else {
            isRepeatPasswordCorreсt = false;
            messageElement = $('<div>').insertAfter(tableElement).attr({
                class: "alert alert-warning alert-dismissible",
                role: "alert"
            });
            messageElement.text(inter.incorrect_repeat_password);
            buttonElement = $('<button>').appendTo(messageElement).attr({
                'type': "button",
                'class': "close",
                'data-dismiss': "alert",
                'aria-label': "Close"
            });
            $('<span>').appendTo(buttonElement).attr("aria-hidden", "true").text('x');
        }
        if (isPasswordCorreсt === true && isRepeatPasswordCorreсt === true) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        if ($('input[name=firstName]').val().match(/(^[А-Яа-я]+)$/)) {
            isfirstNameCorrect = true;
        }
        else {
            isfirstNameCorrect = false;
            messageElement = $('<div>').insertAfter(tableElement).attr({
                class: "alert alert-warning alert-dismissible",
                role: "alert"
            });
            messageElement.text(inter.incorrect_first_name);
            buttonElement = $('<button>').appendTo(messageElement).attr({
                'type': "button",
                'class': "close",
                'data-dismiss': "alert",
                'aria-label': "Close"
            });
            $('<span>').appendTo(buttonElement).attr("aria-hidden", "true").text('x');

        }
        if ($('input[name=lastName]').val().match(/(^[А-Яа-я]+)$/)) {
            isLastNameCorrect = true;
        }
        else {
            isLastNameCorrect = false;
            messageElement = $('<div>').insertAfter(tableElement).attr({
                class: "alert alert-warning alert-dismissible",
                role: "alert"
            });
            messageElement.text(inter.incorrect_last_name);
            buttonElement = $('<button>').appendTo(messageElement).attr({
                'type': "button",
                'class': "close",
                'data-dismiss': "alert",
                'aria-label': "Close"
            });
            $('<span>').appendTo(buttonElement).attr("aria-hidden", "true").text('x');

        }
        if ($('input[name=middleName]').val().match(/(^[А-Яа-я]+)$/)) {
            isMiddleNameCorrect = true;
        }
        else {
            isMiddleNameCorrect = false;
            messageElement = $('<div>').insertAfter(tableElement).attr({
                class: "alert alert-warning alert-dismissible",
                role: "alert"
            });
            messageElement.text(inter.incorrect_middle_name);
            buttonElement = $('<button>').appendTo(messageElement).attr({
                'type': "button",
                'class': "close",
                'data-dismiss': "alert",
                'aria-label': "Close"
            });
            $('<span>').appendTo(buttonElement).attr("aria-hidden", "true").text('x');

        }

        if ($('input[name=email]').val().match(/^[A-Za-z/_0-9/./-]*[@][A-Za-z/_0-9/-]*[.][A-Za-z]{2,3}$/)) {
            isEmailCorreсt = true;
        }
        else {
            isEmailCorreсt = false;
            messageElement = $('<div>').insertAfter(tableElement).attr({
                class: "alert alert-warning alert-dismissible",
                role: "alert"
            });
            messageElement.text(inter.incorrect_email);
            buttonElement = $('<button>').appendTo(messageElement).attr({
                'type': "button",
                'class': "close",
                'data-dismiss': "alert",
                'aria-label': "Close"
            });
            $('<span>').appendTo(buttonElement).attr("aria-hidden", "true").text('x');
        }
        if ($('input[name=phone]').val().match(/^(375)+[0-9]{9}$/)) {
            isPhoneNumberCorreсt = true;
        }
        else {
            isPhoneNumberCorreсt = false;
            messageElement = $('<div>').insertAfter(tableElement).attr({
                class: "alert alert-warning alert-dismissible",
                role: "alert"
            });
            messageElement.text(inter.incorrect_phone);
            buttonElement = $('<button>').appendTo(messageElement).attr({
                'type': "button",
                'class': "close",
                'data-dismiss': "alert",
                'aria-label': "Close"
            });
            $('<span>').appendTo(buttonElement).attr("aria-hidden", "true").text('x');
        }
        if ($('input[name=username]').val().match(/^[a-zA-Z0-9\-\_]{6,}$/)) {
            isUsernameCorreсt = true;
        }
        else {
            isUsernameCorreсt = false;
            messageElement = $('<div>').insertAfter(tableElement).attr({
                class: "alert alert-warning alert-dismissible",
                role: "alert"
            });
            messageElement.text(inter.incorrect_username);
            buttonElement = $('<button>').appendTo(messageElement).attr({
                'type': "button",
                'class': "close",
                'data-dismiss': "alert",
                'aria-label': "Close"
            });
            $('<span>').appendTo(buttonElement).attr("aria-hidden", "true").text('x');
        }
        if (isfirstNameCorrect === true && isLastNameCorrect === true && isMiddleNameCorrect === true &&
                isEmailCorreсt === true && isPhoneNumberCorreсt === true && isUsernameCorreсt === true) {
            return true;
        }
        else {
            return false;
        }

    }
}

//редактирование данных о преподавателе и передача данных на сервер
function editStudent() {
    if (checkValues('edit')) {
        $('#command').val('edit_student');
        $('#form').submit();
    }
}

// функция смены пароля
function changePassword() {
    if (checkValues('password')) {
        $('#command').val('change_password');
        $('#form').submit();
    }
}

//показать курсы преподавателя 
function showStudentCourses(id) {
    $('#command').val('show_student_courses');
    var rowElement = $('#' + id).closest('tr');
    var currentElement = $('<input>').appendTo(rowElement);
    currentElement.attr({
        type: "hidden",
        name: "idStudent",
        value: id
    });
    $('#form').submit();
}

//показать курсы преподавателя 
function showStudentMarks(id) {
    $('#command').val('show_student_marks');
    var rowElement = $('#' + id).closest('tr');
    var currentElement = $('<input>').appendTo(rowElement);
    currentElement.attr({
        type: "hidden",
        name: "idStudent",
        value: id
    });
    $('#form').submit();
}


$('input:checkbox').change(function() {
    if ($(this).is(':checked')) {
        selectedItems = selectedItems + 1;
    }
    else {
        selectedItems = selectedItems - 1;
    }
    if (selectedItems === 1) {
        $('#editButton').removeAttr("disabled", "disabled");
        $('#deleteButton').removeAttr("disabled", "disabled");
        $('#showMarksButton').removeAttr("disabled", "disabled");
        $('#showCoursesButton').removeAttr("disabled", "disabled");
        $('#changePasswordButton').removeAttr("disabled", "disabled");
    }
    else {
        $('#editButton').attr("disabled", "disabled");
        $('#deleteButton').attr("disabled", "disabled");
        $('#showMarksButton').attr("disabled", "disabled");
        $('#showCoursesButton').attr("disabled", "disabled");
        $('#changePasswordButton').attr("disabled", "disabled");
    }
});

function logout() {
    $('#command').val('logout');
    $('#form').submit();
}

function back (value) {
    document.getElementById("command").value = value;
    document.getElementById("form").submit();
}