window.isfirstNameCorrect = false;
window.isLastNameCorrect = false;
window.isMiddleNameCorrect = false;
window.isEmailCorreсt = false;
window.isPhoneNumberCorreсt = false;
window.isPasswordCorreсt = false;
window.isRepeatPasswordCorrect = false;
window.selectedItems = 0;
window.itemsText = [];
window.itemNames = ['firstName', 'lastName', 'middleName', 'phone', 'email'];


//навигация меню
function navigate(value, currentElement)
{
    var activeElement = $(".active");
    $(activeElement).toggleClass("active");
    $(currentElement).toggleClass("active");
    document.getElementById("command").value = value;
    document.getElementById("form").submit();
}


function chnageProfile() {
    var currentElement;
    $('#changeProfileDataButton').attr({
        disabled: "disabled",
        style: "visibility: hidden;"
    });
    $('#changePasswordButton').attr({
        disabled: "disabled",
        style: "visibility: hidden;"
    });
    for (var i = 0; i < itemNames.length; i++) {
        currentElement = $('#' + itemNames[i] + '');
        itemsText[i] = currentElement.text();
        currentElement.text("");
        currentElement.append($('<input>').attr({
            type: "text",
            class: "form-control editElement",
            name: itemNames[i]
        }));
        currentElement.children().val(itemsText[i]);
    }

    var saveButton = $('<input>').appendTo($('#studentProfile'));
    saveButton.attr({
        type: "button",
        value: inter.save_button,
        class: "btn btn-primary editElement",
        style: "margin: 5px;",
        onclick: "editProfile();"
    });

    var cancelButton = $('<input>').appendTo($('#studentProfile'));
    cancelButton.attr({
        type: "button",
        value: inter.cancel_button,
        class: "btn btn-default editElement",
        onclick: "cancelEdit();"
    });
}

//отмена редактирования, возврат к предыдущему состоянию
function cancelEdit() {
    for (var i = 0; i < itemNames.length; i++) {
        currentElement = $('#' + itemNames[i] + '');
        currentElement.text(itemsText[i]);
        $(".editElement").remove();
    }
    $('#changeProfileDataButton').removeAttr("disabled");
    $('#changeProfileDataButton').removeAttr("style");
    $('#changePasswordButton').removeAttr("disabled");
    $('#changePasswordButton').removeAttr("style");
}

//добавление интерфейса для смены пароля
function addChangePasswordFields() {
    var currentElement;
    $('#changeProfileDataButton').attr({
        disabled: "disabled",
        style: "visibility: hidden;"
    });
    $('#changePasswordButton').attr({
        disabled: "disabled",
        style: "visibility: hidden;"
    });
    var divElement = $('<div>').appendTo($('#studentProfile'));
    divElement.attr("class", "form-group changePasswordElements");
    currentElement = $('<input>').appendTo(divElement);
    currentElement.attr({
        class: "form-control changePasswordElements",
        type: "password",
        name: "password",
        placeholder: inter.enter_password
    });
    currentElement = $('<input>').appendTo(divElement);
    currentElement.attr({
        class: "form-control changePasswordElements",
        type: "password",
        name: "repeatPassword",
        placeholder: inter.repeat_enter_password
    });

    $('<br>').insertAfter(divElement);
    var cancelButton = $('<input>').insertAfter(divElement);
    cancelButton.attr({
        type: "button",
        value: inter.cancel_button,
        class: "btn btn-default changePasswordElements",
        onclick: "cancelChangePassword();"
    });
    var saveButton = $('<input>').insertAfter(divElement);
    saveButton.attr({
        type: "button",
        value: inter.save_button,
        class: "btn btn-primary changePasswordElements",
        style: "margin: 5px;",
        onclick: "changePassword();"
    });
}

function cancelChangePassword() {
    $(".changePasswordElements").remove();
    $('#changeProfileDataButton').removeAttr("disabled");
    $('#changeProfileDataButton').removeAttr("style");
    $('#changePasswordButton').removeAttr("disabled");
    $('#changePasswordButton').removeAttr("style");
}

//проверка значений введеных пользователем
function checkValues(functionName) {
    var formElement = $('#form');
    var messageElement;
    var buttonElement;
    if (functionName === 'password') {
        if ($('input[name=password]').val().match(/^.{8,16}$/)) {
            isPasswordCorreсt = true;
        }
        else {
            isPasswordCorreсt = false;
            messageElement = $('<div>').insertAfter(formElement).attr({
                class: "alert alert-warning alert-dismissible",
                role: "alert"
            });
            messageElement.text(inter.incorrect_password);
            buttonElement = $('<button>').appendTo(messageElement).attr({
                'type': "button",
                'class': "close",
                'data-dismiss': "alert",
                'aria-label': "Close"
            });
            $('<span>').appendTo(buttonElement).attr("aria-hidden", "true").text('x');
        }
        if ($('input[name=repeatPassword]').val() === $('input[name=password]').val()) {
            isRepeatPasswordCorreсt = true;
        }
        else {
            isRepeatPasswordCorreсt = false;
            messageElement = $('<div>').insertAfter(formElement).attr({
                class: "alert alert-warning alert-dismissible",
                role: "alert"
            });
            messageElement.text(inter.incorrect_repeat_password);
            buttonElement = $('<button>').appendTo(messageElement).attr({
                'type': "button",
                'class': "close",
                'data-dismiss': "alert",
                'aria-label': "Close"
            });
            $('<span>').appendTo(buttonElement).attr("aria-hidden", "true").text('x');
        }
        if (isPasswordCorreсt === true && isRepeatPasswordCorreсt === true) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        if ($('input[name=firstName]').val().match(/(^[А-Яа-я]+)$/)) {
            isfirstNameCorrect = true;
        }
        else {
            isfirstNameCorrect = false;
            messageElement = $('<div>').insertAfter(formElement).attr({
                class: "alert alert-warning alert-dismissible",
                role: "alert"
            });
            messageElement.text(inter.incorrect_first_name);
            buttonElement = $('<button>').appendTo(messageElement).attr({
                'type': "button",
                'class': "close",
                'data-dismiss': "alert",
                'aria-label': "Close"
            });
            $('<span>').appendTo(buttonElement).attr("aria-hidden", "true").text('x');

        }
        if ($('input[name=lastName]').val().match(/(^[А-Яа-я]+)$/)) {
            isLastNameCorrect = true;
        }
        else {
            isLastNameCorrect = false;
            messageElement = $('<div>').insertAfter(formElement).attr({
                class: "alert alert-warning alert-dismissible",
                role: "alert"
            });
            messageElement.text(inter.incorrect_last_name);
            buttonElement = $('<button>').appendTo(messageElement).attr({
                'type': "button",
                'class': "close",
                'data-dismiss': "alert",
                'aria-label': "Close"
            });
            $('<span>').appendTo(buttonElement).attr("aria-hidden", "true").text('x');

        }
        if ($('input[name=middleName]').val().match(/(^[А-Яа-я]+)$/)) {
            isMiddleNameCorrect = true;
        }
        else {
            isMiddleNameCorrect = false;
            messageElement = $('<div>').insertAfter(formElement).attr({
                class: "alert alert-warning alert-dismissible",
                role: "alert"
            });
            messageElement.text(inter.incorrect_middle_name);
            buttonElement = $('<button>').appendTo(messageElement).attr({
                'type': "button",
                'class': "close",
                'data-dismiss': "alert",
                'aria-label': "Close"
            });
            $('<span>').appendTo(buttonElement).attr("aria-hidden", "true").text('x');

        }

        if ($('input[name=email]').val().match(/^[A-Za-z/_0-9/./-]*[@][A-Za-z/_0-9/-]*[.][A-Za-z]{2,3}$/)) {
            isEmailCorreсt = true;
        }
        else {
            isEmailCorreсt = false;
            messageElement = $('<div>').insertAfter(formElement).attr({
                class: "alert alert-warning alert-dismissible",
                role: "alert"
            });
            messageElement.text(inter.incorrect_email);
            buttonElement = $('<button>').appendTo(messageElement).attr({
                'type': "button",
                'class': "close",
                'data-dismiss': "alert",
                'aria-label': "Close"
            });
            $('<span>').appendTo(buttonElement).attr("aria-hidden", "true").text('x');
        }
        if ($('input[name=phone]').val().match(/^(375)+[0-9]{9}$/)) {
            isPhoneNumberCorreсt = true;
        }
        else {
            isPhoneNumberCorreсt = false;
            messageElement = $('<div>').insertAfter(formElement).attr({
                class: "alert alert-warning alert-dismissible",
                role: "alert"
            });
            messageElement.text(inter.incorrect_phone);
            buttonElement = $('<button>').appendTo(messageElement).attr({
                'type': "button",
                'class': "close",
                'data-dismiss': "alert",
                'aria-label': "Close"
            });
            $('<span>').appendTo(buttonElement).attr("aria-hidden", "true").text('x');
        }
        if (isfirstNameCorrect === true && isLastNameCorrect === true && isMiddleNameCorrect === true &&
                isEmailCorreсt === true && isPhoneNumberCorreсt === true) {
            return true;
        }
        else {
            return false;
        }
    }
}

//редактирование данных о студенте и передача данных на сервер
function editProfile() {
    if (checkValues('edit')) {
        $('#command').val('edit_student');
        $('#form').submit();
    }
}

//записаться на курсы
function enrollStudentToCourse(clickedButton) {
    var idButton = clickedButton.id;
    ($('<input>')).insertAfter(clickedButton).attr({
        class: "editElements",
        type: "hidden",
        name: "idCourse",
        value: idButton
    });
    $('#command').val('enroll_student_to_course');
    $('#form').submit();
}


// функция смены пароля
function changePassword() {
    if (checkValues('password')) {
        $('#command').val('change_password');
        $('#form').submit();
    }
}
function logout() {
    $('#command').val('logout');
    $('#form').submit();
}