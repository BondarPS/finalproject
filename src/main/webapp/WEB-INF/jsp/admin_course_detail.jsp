<%-- 
    Document   : main
    Created on : 12.08.2015, 17:49:19
    Author     : Pavel
--%>

<%@ page language="java" contentType="text/html; charset=utf-8"  pageEncoding="utf-8"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%> 
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <fmt:setLocale value="${sessionScope.locale}" /> 
        <fmt:setBundle basename="prop" var="loc" /> 
        <fmt:message bundle="${loc}" key="prop.administrator" var="administrator" /> 
        <fmt:message bundle="${loc}" key="prop.menu_courses" var="menu_courses" /> 
        <fmt:message bundle="${loc}" key="prop.menu_teachers" var="menu_teachers" /> 
        <fmt:message bundle="${loc}" key="prop.menu_students" var="menu_students" /> 
        <fmt:message bundle="${loc}" key="prop.button_logout" var="button_logout" />
        <fmt:message bundle="${loc}" key="prop.course_name" var="course_name" /> 
        <fmt:message bundle="${loc}" key="prop.is_end" var="is_end" /> 
        <fmt:message bundle="${loc}" key="prop.yes" var="yes" /> 
        <fmt:message bundle="${loc}" key="prop.no" var="no" /> 
        <fmt:message bundle="${loc}" key="prop.course_information" var="course_information" /> 
        <fmt:message bundle="${loc}" key="prop.period" var="period" /> 
        <fmt:message bundle="${loc}" key="prop.teacher_label" var="teacher_label" /> 
        <fmt:message bundle="${loc}" key="prop.teacher_not_selected" var="teacher_not_selected" /> 
        <fmt:message bundle="${loc}" key="prop.choose_teacher" var="choose_teacher" /> 
        <fmt:message bundle="${loc}" key="prop.appoint_teacher" var="appoint_teacher" /> 
        <fmt:message bundle="${loc}" key="prop.firstName" var="firstName" /> 
        <fmt:message bundle="${loc}" key="prop.lastName" var="lastName" /> 
        <fmt:message bundle="${loc}" key="prop.middleName" var="middleName" /> 
        <fmt:message bundle="${loc}" key="prop.email" var="email" /> 
        <fmt:message bundle="${loc}" key="prop.phone" var="phone" /> 
        <fmt:message bundle="${loc}" key="prop.author" var="author" /> 

        <title>${administrator}</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/offcanvas.css" rel="stylesheet">

        <link href="css/main.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        <div class="pull-right">
            <input class="btn btn-default btn-sm" type="button" value="${button_logout}" onclick="logout();"/>
        </div>
        <div class="container">
            <div class="row row-offcanvas row-offcanvas-right">
                <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
                    <div id="right-menu" class="list-group">
                        <span class="list-group-item active" onclick="navigate('show_courses', this);">${menu_courses}</span>
                        <span class="list-group-item" onclick="navigate('show_admin_teachers', this);">${menu_teachers}</span>
                        <span class="list-group-item" onclick="navigate('show_admin_students', this);">${menu_students}</span>
                    </div>
                </div><!--/.sidebar-offcanvas-->  
                <div class="col-xs-12 col-sm-9">
                    <p class="pull-right visible-xs">
                        <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas"></button>
                    </p>
                    <div class="row">
                        <form id="form" method="post" action="controller" style="margin-bottom: 5px;">
                            <input id="command" type="hidden" name="command" value="show_admin_courses"/>
                            <c:if test="${ not empty course}">
                                <h3 class="text-center">${course_information}</h3>
                                <h4>${course_name}:</h4> ${course.courseName}, 
                                <h4>${period}:</h4> ${course.startDate} - ${course.endDate} 
                                <h4>${is_end}:</h4> 
                                <c:choose>
                                    <c:when test="${course.ended == true}">${yes}</c:when>
                                    <c:otherwise>${no}</c:otherwise>
                                </c:choose>
                                <input type="hidden" value="${course.id}" name="idCourse"/>
                            </c:if>
                            <c:choose>
                                <c:when test="${ not empty teacher}">
                                    <div>
                                        <h4>${teacher_label}:</h4> ${teacher.lastName} ${teacher.firstName} ${teacher.middleName} 
                                        <br/>${teacher.phoneNumber}, ${teacher.email}
                                    </div>
                                </c:when>
                                <c:otherwise>
                                    <br/><h4>${teacher_not_selected}</h4>
                                    <div class="form-inline">
                                        <h5>${choose_teacher}</h5>
                                        <select name="idTeacher" class="form-control">
                                            <c:forEach var="teacherItem" items="${teachers}" varStatus="iter">
                                                <option value="${teacherItem.id}">${teacherItem.lastName} ${teacherItem.firstName} ${teacherItem.middleName}</option>
                                            </c:forEach>
                                        </select>
                                        <input type="button" class="btn btn-primary btn-xs" value="${appoint_teacher}" onclick="appointTeacher();"/>
                                    </div>
                                </c:otherwise>
                            </c:choose>
                            <br/>
                            <div class="table-responsive">
                                <table id="coursesTable" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>${firstName}</th>
                                            <th>${lastName}</th>
                                            <th>${middleName}</th>
                                            <th>${email}</th>
                                            <th>${phone}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="studentItem" items="${students}" varStatus="iter">
                                            <tr>
                                        <input id="${studentItem.id}" type="hidden"/>
                                        <td>${studentItem.firstName}</td>
                                        <td>${studentItem.lastName}</td>
                                        <td>${studentItem.middleName}</td>
                                        <td>${studentItem.email}</td>
                                        <td>${studentItem.phoneNumber}</td>                                         
                                        </tr>
                                    </c:forEach>
                                </table> 
                                <input type="button" class="btn btn-sm btn-default" value="Назад" onclick="back('show_courses');"/>
                                <c:if test="${ not empty message}">
                                    <div class="alert alert-success alert-dismissible" role="alert">
                                        ${message}
                                        <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">x</span>
                                        </button>
                                    </div>
                                </c:if>
                                <c:if test="${ not empty messages}">
                                    <c:forEach var="messageItem" items="${messages}" varStatus="iter">
                                        <div class="alert alert-warning alert-dismissible" role="alert">
                                            ${messageItem}
                                            <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">x</span>
                                            </button>
                                        </div>
                                    </c:forEach>
                                </c:if>
                            </div>
                        </form>

                    </div><!--/row-->
                </div><!--/.col-xs-12.col-sm-9-->
            </div><!--/row-->

            <hr>

            <footer>
                <p>&copy; ${author}</p>
            </footer>

        </div><!--/.container-->


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="js/jquery-1.11.0.js"></script>
        <script src="js/bootstrap.min.js"></script>

        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="js/ie10-viewport-bug-workaround.js"></script>
        <script src="js/offcanvas.js"></script>
        <script src="js/messages-${locale}.js"></script>
        <script src="js/admin_course_detail.js"></script>
    </body>
</html>


