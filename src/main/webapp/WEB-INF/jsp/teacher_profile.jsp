<%-- 
    Document   : main
    Created on : 12.08.2015, 17:49:19
    Author     : Pavel
--%>

<%@ page language="java" contentType="text/html; charset=utf-8"  pageEncoding="utf-8"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%> 
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <fmt:setLocale value="${sessionScope.locale}" /> 
        <fmt:setBundle basename="prop" var="loc" /> 
        <fmt:message bundle="${loc}" key="prop.teacher_label" var="teacher_label" /> 
        <fmt:message bundle="${loc}" key="prop.menu_courses" var="menu_courses" /> 
        <fmt:message bundle="${loc}" key="prop.menu_profile" var="menu_profile" /> 
        <fmt:message bundle="${loc}" key="prop.menu_marks" var="menu_marks" /> 
        <fmt:message bundle="${loc}" key="prop.button_logout" var="button_logout" />
        <fmt:message bundle="${loc}" key="prop.greeting" var="greeting" /> 
        <fmt:message bundle="${loc}" key="prop.firstName" var="firstName" /> 
        <fmt:message bundle="${loc}" key="prop.lastName" var="lastName" /> 
        <fmt:message bundle="${loc}" key="prop.middleName" var="middleName" /> 
        <fmt:message bundle="${loc}" key="prop.login" var="login" /> 
        <fmt:message bundle="${loc}" key="prop.profile_data" var="profile_data" /> 
        <fmt:message bundle="${loc}" key="prop.contact_data" var="contact_data" /> 
        <fmt:message bundle="${loc}" key="prop.phone" var="phone" /> 
        <fmt:message bundle="${loc}" key="prop.email" var="email" /> 
        <fmt:message bundle="${loc}" key="prop.change_password_button" var="change_password_button" /> 
        <fmt:message bundle="${loc}" key="prop.email" var="email" /> 
        <fmt:message bundle="${loc}" key="prop.change_data" var="change_data" /> 
        <fmt:message bundle="${loc}" key="prop.author" var="author" /> 
        <title>${teacher_label}</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/offcanvas.css" rel="stylesheet">

        <link href="css/main.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        <div class="pull-right">
            <input class="btn btn-default btn-sm" type="button" value="${button_logout}" onclick="logout();"/>
        </div>
        <div class="container">
            <div class="row row-offcanvas row-offcanvas-right">
                <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
                    <div id="right-menu" class="list-group">
                        <span class="list-group-item active" onclick="navigate('show_profile', this);">${menu_profile}</span>
                        <span class="list-group-item" onclick="navigate('show_courses', this);">${menu_courses}</span>
                    </div>
                </div><!--/.sidebar-offcanvas-->  
                <div class="col-xs-12 col-sm-9">
                    <p class="pull-right visible-xs">
                        <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas"></button>
                    </p>
                    <div class="row">
                        <form id="form" method="post" action="controller" style="margin-bottom: 5px;">
                            <input id="command" type="hidden" name="command" value="show_admin_courses"/>
                            <div id="studentProfile" class="form-group">
                                <c:if test="${ not empty teacher}">
                                    <h2 class="text-center">${greeting},${teacher.firstName} ${teacher.middleName} </h2>
                                    <h3>${profile_data}</h3>
                                    <h4>${firstName} </h4><span id="firstName">${teacher.firstName}</span>
                                    <h4>${lastName} </h4><span id="lastName">${teacher.lastName}</span>
                                    <h4>${middleName} </h4><span id="middleName">${teacher.middleName}</span>
                                    <h4>${login} </h4><span id="username">${teacher.user.username}</span>
                                    <br/><h3>${contact_data}</h3>
                                    <h4>${phone} </h4><span id="phone">${teacher.phoneNumber}</span>
                                    <h4>${email} </h4><span id="email">${teacher.email}</span>
                                    <br>
                                    <input id="changeProfileDataButton" type="button" value="${change_data}" class="btn btn-primary btn-md" onclick="chnageProfile();"/>
                                    <input id="changePasswordButton" type="button" value="${change_password_button}" class="btn btn-primary btn-md" onclick="addChangePasswordFields();"/>
                                    <input type="hidden" value="${teacher.id}" name="idTeacher"/>
                                </c:if>
                            </div>
                            <c:if test="${ not empty message}">
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    ${message}
                                    <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">x</span>
                                    </button>
                                </div>
                            </c:if>
                            <c:if test="${ not empty messages}">
                                <c:forEach var="messageItem" items="${messages}" varStatus="iter">
                                    <div class="alert alert-warning alert-dismissible" role="alert">
                                        ${messageItem}
                                        <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">x</span>
                                        </button>
                                    </div>
                                </c:forEach>
                            </c:if>
                        </form>                    
                    </div><!--/row-->
                </div><!--/.col-xs-12.col-sm-9-->
            </div><!--/row-->

            <hr>

            <footer>
                <p>&copy; ${author}</p>
            </footer>

        </div><!--/.container-->


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="js/jquery-1.11.0.js"></script>
        <script src="js/bootstrap.min.js"></script>

        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="js/ie10-viewport-bug-workaround.js"></script>
        <script src="js/offcanvas.js"></script>
        <script src="js/messages-${locale}.js"></script>
        <script src="js/teacher_profile.js"></script>
    </body>
</html>

