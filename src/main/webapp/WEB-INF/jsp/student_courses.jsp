<%-- 
    Document   : main
    Created on : 12.08.2015, 17:49:19
    Author     : Pavel
--%>

<%@ page language="java" contentType="text/html; charset=utf-8"  pageEncoding="utf-8"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%> 
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <fmt:setLocale value="${sessionScope.locale}" /> 
        <fmt:setBundle basename="prop" var="loc" /> 
        <fmt:message bundle="${loc}" key="prop.student_label" var="student_label" /> 
        <fmt:message bundle="${loc}" key="prop.menu_courses" var="menu_courses" /> 
        <fmt:message bundle="${loc}" key="prop.menu_profile" var="menu_profile" /> 
        <fmt:message bundle="${loc}" key="prop.menu_marks" var="menu_marks" /> 
        <fmt:message bundle="${loc}" key="prop.button_logout" var="button_logout" />
        <fmt:message bundle="${loc}" key="prop.course_name" var="course_name" /> 
        <fmt:message bundle="${loc}" key="prop.start_date" var="start_date" /> 
        <fmt:message bundle="${loc}" key="prop.end_date" var="end_date" /> 
        <fmt:message bundle="${loc}" key="prop.is_end" var="is_end" /> 
        <fmt:message bundle="${loc}" key="prop.yes" var="yes" /> 
        <fmt:message bundle="${loc}" key="prop.no" var="no" /> 
        <fmt:message bundle="${loc}" key="prop.course_ended" var="course_ended" /> 
        <fmt:message bundle="${loc}" key="prop.my_courses_header" var="my_courses_header" /> 
        <fmt:message bundle="${loc}" key="prop.enroll_to_course" var="enroll_to_course" /> 
        <fmt:message bundle="${loc}" key="prop.author" var="author" /> 
        <title>${student_label}</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/offcanvas.css" rel="stylesheet">

        <link href="css/main.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        <div class="pull-right">
            <input class="btn btn-default btn-sm" type="button" value="${button_logout}" onclick="logout();"/>
        </div>
        <div class="container">
            <div class="row row-offcanvas row-offcanvas-right">
                <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
                    <div id="right-menu" class="list-group">
                        <span class="list-group-item" onclick="navigate('show_profile', this);">${menu_profile}</span>
                        <span class="list-group-item active" onclick="navigate('show_courses', this);">${menu_courses}</span>
                        <span class="list-group-item" onclick="navigate('show_student_marks', this);">${menu_marks}</span>
                    </div>
                </div><!--/.sidebar-offcanvas-->  
                <div class="col-xs-12 col-sm-9">
                    <p class="pull-right visible-xs">
                        <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas"></button>
                    </p>
                    <div class="row">
                        <form id="form" method="post" action="controller" style="margin-bottom: 5px;">
                            <input id="command" type="hidden" name="command" value="show_courses"/>
                            <div class="table-responsive">
                                <table id="coursesTable" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>${course_name}</th>
                                            <th>${start_date}</th>
                                            <th>${end_date}</th>
                                            <th>${is_end}</th>
                                            <th>#</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="courseItem" items="${courses}" varStatus="iter">
                                            <tr>
                                                <td>${courseItem.courseName}</td>
                                                <td>${courseItem.startDate}</td>
                                                <td>${courseItem.endDate}</td>
                                                <c:choose>
                                                    <c:when test="${courseItem.ended == true}">
                                                        <td>${yes}</td>
                                                        <th>${course_ended}</th>
                                                        </c:when>
                                                        <c:otherwise>
                                                        <td>${no}</td>
                                                        <th>
                                                            <input id="${courseItem.id}" type="button"  value="${enroll_to_course}" class="btn btn-primary btn-xs" onclick="enrollStudentToCourse(this);"/>
                                                        </th>
                                                    </c:otherwise>
                                                </c:choose>

                                            </tr>
                                        </c:forEach>
                                </table> 
                                <h2>${my_courses_header}</h2>
                                <table id="coursesTable" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>${course_name}</th>
                                            <th>${start_date}</th>
                                            <th>${end_date}</th>
                                            <th>${is_end}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="courseItem" items="${studentCourses}" varStatus="iter">
                                            <tr>
                                                <td>${courseItem.courseName}</td>
                                                <td>${courseItem.startDate}</td>
                                                <td>${courseItem.endDate}</td>
                                                <c:choose>
                                                    <c:when test="${courseItem.ended == true}"><td>${yes}</td></c:when>
                                                    <c:otherwise><td>${no}</td></c:otherwise>
                                                </c:choose>                                            
                                            </tr>
                                        </c:forEach>
                                </table> 
                                <c:if test="${ not empty message}">
                                    <div class="alert alert-success alert-dismissible" role="alert">
                                        ${message}
                                        <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">x</span>
                                        </button>
                                    </div>
                                </c:if>
                                <c:if test="${ not empty messages}">
                                    <c:forEach var="messageItem" items="${messages}" varStatus="iter">
                                        <div class="alert alert-warning alert-dismissible" role="alert">
                                            ${messageItem}
                                            <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">x</span>
                                            </button>
                                        </div>
                                    </c:forEach>
                                </c:if>
                            </div>
                        </form>
                    </div><!--/row-->
                </div><!--/.col-xs-12.col-sm-9-->
            </div><!--/row-->

            <hr>

            <footer>
                <p>&copy; ${author}</p>
            </footer>

        </div><!--/.container-->


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="js/jquery-1.11.0.js"></script>
        <script src="js/bootstrap.min.js"></script>

        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="js/ie10-viewport-bug-workaround.js"></script>
        <script src="js/offcanvas.js"></script>
        <script src="js/messages-${locale}.js"></script>
        <script src="js/student_profile.js"></script>
    </body>
</html>

