/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.controller;

/**
 *
 * @author Pavel
 */
public class JspPageName {

    private JspPageName() {
    }
    public static final String LOGIN_PAGE = "login.jsp";
    public static final String ERROR_PAGE = "error.jsp";
    public static final String REGISTER_PAGE = "registration.jsp";
    public static final String STUDENT_PROFILE = "WEB-INF/jsp/student_profile.jsp";
    public static final String TEACHER_PROFILE = "WEB-INF/jsp/teacher_profile.jsp";
    public static final String ADMIN_COURSES = "WEB-INF/jsp/admin_courses.jsp";
    public static final String ADMIN_TEACHERS = "WEB-INF/jsp/admin_teachers.jsp";
    public static final String ADMIN_STUDENTS = "WEB-INF/jsp/admin_students.jsp";
    public static final String ADMIN_COURSE_DETAIL  = "WEB-INF/jsp/admin_course_detail.jsp";
    public static final String ADMIN_TEACHER_COURSES  = "WEB-INF/jsp/admin_teacher_courses.jsp";
    public static final String ADMIN_STUDENT_COURSES  = "WEB-INF/jsp/admin_student_courses.jsp";
    public static final String ADMIN_STUDENT_MARKS  = "WEB-INF/jsp/admin_student_marks.jsp";
    public static final String STUDENT_COURSES  = "WEB-INF/jsp/student_courses.jsp";
    public static final String STUDENT_MARKS  = "WEB-INF/jsp/student_marks.jsp";
    public static final String TEACHER_COURSES  = "WEB-INF/jsp/teacher_courses.jsp";
    public static final String TEACHER_COURSE_MARKS  = "WEB-INF/jsp/teacher_course_marks.jsp";
}
