/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.epam.finalproject.controller;

/**
 *
 * @author Pavel
 */
public class RequestParameterName {

    private RequestParameterName() {
    }
    
    public static final String USER_NAME = "username";
    public static final String SIMPLE_INFO = "simpleinfo";
    public static final String PASSWORD = "password";
    public static final String REPEAT_PASSWORD = "repeatPassword";
    public static final String COMMAND_NAME = "command";
    public static final String CLIENT_TYPE = "clientType";
    public static final String MESSAGE = "message";
    public static final String MESSAGES = "messages";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String MIDDLE_NAME = "middleName";
    public static final String EMAIL = "email";
    public static final String PHONE = "phone";
    public static final String ID_TEACHER = "idTeacher";
    public static final String ID_STUDENT = "idStudent";
    public static final String LOCALE = "locale";
    public static final String PAGE = "page";
    
    
    public static final String COURSES = "courses";
    public static final String STUDENT_COURSES = "studentCourses";
    public static final String COURSE = "course";
    public static final String ID_COURSE = "idCourse";
    public static final String COURSE_NAME = "courseName";
    public static final String START_DATE = "startDate";
    public static final String END_DATE = "endDate";
    public static final String IS_ENDED = "isEnded";
    
    public static final String STUDENT = "student";
    public static final String STUDENTS = "students";
    public static final String TEACHERS = "teachers";
    public static final String TEACHER = "teacher";
    public static final String MARKS = "marks";
    
    public static final String ID_MARK = "idMark";
    public static final String MARK = "mark";
    public static final String COMMENTS = "comments";
    
    
    public static final String FIRST_NAME_MESSAGE = "firstNameMessage";
    public static final String LAST_NAME_MESSAGE = "lastNameMessage";
    public static final String MIDDLE_NAME_MESSAGE = "middleNameMessage";
    public static final String EMAIL_MESSAGE = "emailMessage";
    public static final String PHONE_MESSAGE = "phoneMessage";
    public static final String USER_NAME_MESSAGE = "userNameMessage";
    public static final String PASSWORD_MESSAGE = "passwordMessage";   
}

