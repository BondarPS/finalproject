/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.model;

/**
 *
 * @author Pavel
 */
public class Mark {

    private int id;
    private Student student;
    private Course course;
    private int mark;
    private String comments;

    public Mark() {
    }

    public int getId() {
        return id;
    }

    public int getMark() {
        return mark;
    }

    public String getComments() {
        return comments;
    }

    public Course getCourse() {
        return course;
    }

    public Student getStudent() {
        return student;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

}
