/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.epam.finalproject.model;

/**
 *
 * @author Pavel
 */
public class StudentCourse {
    private int id;
    private int idStudent;
    private int idCourse;

    public StudentCourse() {
    }

    public int getId() {
        return id;
    }

    public int getIdStudent() {
        return idStudent;
    }

    public int getIdCourse() {
        return idCourse;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    public void setIdCourse(int idCourse) {
        this.idCourse = idCourse;
    }
    
    
}
