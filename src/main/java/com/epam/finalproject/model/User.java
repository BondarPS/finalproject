/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.model;

/**
 *
 * @author Pavel
 */
public class User {

    private int id;
    private int idRole;
    private String Username;
    private String Password;
    

    public int getId() {
        return id;
    }
    
    public int getIdRole() {
        return idRole;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public void setIdRole(int idRole) {
        this.idRole = idRole;
    }

    public String getUsername() {
        return Username;
    }

    public String getPassword() {
        return Password;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

}
