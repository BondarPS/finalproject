/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.epam.finalproject.model;

/**
 *
 * @author Pavel
 */
public class TeacherCourse {
    private int id;
    private int idTeacher;
    private int idCourse;

    public TeacherCourse() {
    }

    public int getId() {
        return id;
    }

    public int getIdTeacher() {
        return idTeacher;
    }

    public int getIdCourse() {
        return idCourse;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIdTeacher(int idTeacher) {
        this.idTeacher = idTeacher;
    }

    public void setIdCourse(int idCourse) {
        this.idCourse = idCourse;
    }
    
    
}
