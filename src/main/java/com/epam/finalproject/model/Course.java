/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.epam.finalproject.model;

import java.util.Date;

/**
 *
 * @author Pavel
 */
public class Course {
    private int id;
    private Date startDate;
    private Date endDate;
    private String courseName;
    private boolean ended;
    private Teacher teacher;

    public Course() {
    }

    public int getId() {
        return id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public String getCourseName() {
        return courseName;
    }

    public boolean isEnded() {
        return ended;
    }

    public Teacher getTeacher() {
        return teacher;
    }
    
    
    
    public void setId(int id) {
        this.id = id;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public void setEnded(boolean ended) {
        this.ended = ended;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    
    
}
