/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Pavel
 */
public class ParameterValidator {

    public boolean checkInt(String value) {
        if (value == null || value.length() == 0) {
            return false;
        }
        int i = 0;
        if (value.charAt(0) == '-') {
            if (value.length() == 1) {
                return false;
            }
            i = 1;
        }

        char c;
        for (; i < value.length(); i++) {
            c = value.charAt(i);
            if (!(c >= '0' && c <= '9')) {
                return false;
            }
        }
        return true;
    }

    public boolean checkStringOnMatch(String value, String reg) {
        if (value == null) {
            return false;
        } else {
            Pattern pattern = Pattern.compile(reg);
            Matcher matcher = pattern.matcher(value);
            return matcher.matches();
        }
    }
}
