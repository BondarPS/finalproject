/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.service;

import com.epam.finalproject.service.impl.AddCourseCommand;
import com.epam.finalproject.service.impl.AddTeacherCommand;
import com.epam.finalproject.service.impl.AppointTeacherCommand;
import com.epam.finalproject.service.impl.ChangeLocaleCommand;
import com.epam.finalproject.service.impl.ChangePasswordCommand;
import com.epam.finalproject.service.impl.EditCourseCommand;
import com.epam.finalproject.service.impl.EditStudentCommand;
import com.epam.finalproject.service.impl.EditTeacherCommand;
import com.epam.finalproject.service.impl.EnrollStudentToCourseCommand;
import com.epam.finalproject.service.impl.LoginCommand;
import com.epam.finalproject.service.impl.LogoutCommand;
import com.epam.finalproject.service.impl.NoSuchCommand;
import com.epam.finalproject.service.impl.PutMarkCommand;
import com.epam.finalproject.service.impl.RegisterCommand;
import com.epam.finalproject.service.impl.ShowAdminStudentsCommand;
import com.epam.finalproject.service.impl.ShowAdminTeachersCommand;
import com.epam.finalproject.service.impl.ShowCourseStudentsCommand;
import com.epam.finalproject.service.impl.ShowDetailCourseInformationCommand;
import com.epam.finalproject.service.impl.ShowCoursesCommand;
import com.epam.finalproject.service.impl.ShowStudentMarksCommand;
import com.epam.finalproject.service.impl.ShowProfileCommand;
import com.epam.finalproject.service.impl.ShowStudentCoursesCommand;
import com.epam.finalproject.service.impl.ShowTeacherCoursesCommand;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Pavel
 */
public class CommandHelper {

    private static final CommandHelper instance = new CommandHelper();

    private Map<CommandName, ICommand> commands = new HashMap<>();

    public CommandHelper() {
        commands.put(CommandName.LOGIN, new LoginCommand());
        commands.put(CommandName.CHANGE_LOCALE, new ChangeLocaleCommand());
        commands.put(CommandName.LOGOUT, new LogoutCommand());
        commands.put(CommandName.NO_SUCH_COMMAND, new NoSuchCommand());
        commands.put(CommandName.REGISTER, new RegisterCommand());
        commands.put(CommandName.SHOW_ADMIN_TEACHERS, new ShowAdminTeachersCommand());
        commands.put(CommandName.SHOW_ADMIN_STUDENTS, new ShowAdminStudentsCommand());
        commands.put(CommandName.ADD_COURSE, new AddCourseCommand());
        commands.put(CommandName.ADD_TEACHER, new AddTeacherCommand());
        commands.put(CommandName.EDIT_TEACHER, new EditTeacherCommand());
        commands.put(CommandName.EDIT_COURSE, new EditCourseCommand());
        commands.put(CommandName.CHANGE_PASSWORD, new ChangePasswordCommand());
        commands.put(CommandName.SHOW_DETAIL_COURSE_INFORMATION, new ShowDetailCourseInformationCommand());
        commands.put(CommandName.APPOINT_TEACHER, new AppointTeacherCommand());
        commands.put(CommandName.SHOW_TEACHER_COURSES, new ShowTeacherCoursesCommand());
        commands.put(CommandName.EDIT_STUDENT, new EditStudentCommand());
        commands.put(CommandName.SHOW_COURSES, new ShowCoursesCommand());
        commands.put(CommandName.SHOW_STUDENT_MARKS, new ShowStudentMarksCommand());
        commands.put(CommandName.SHOW_STUDENT_COURSES, new ShowStudentCoursesCommand());
        commands.put(CommandName.SHOW_PROFILE, new ShowProfileCommand());
        commands.put(CommandName.ENROLL_STUDENT_TO_COURSE, new EnrollStudentToCourseCommand());
        commands.put(CommandName.SHOW_COURSE_STUDENTS, new ShowCourseStudentsCommand());
        commands.put(CommandName.PUT_MARK, new PutMarkCommand());
    }

    public static CommandHelper getInstance() {
        return instance;
    }

    public ICommand getCommand(String commandName) {
        CommandName name = CommandName.valueOf(commandName.toUpperCase());
        ICommand command;
        if (null != name) {
            command = commands.get(name);
        } else {
            command = commands.get(CommandName.NO_SUCH_COMMAND);
        }
        return command;
    }
}
