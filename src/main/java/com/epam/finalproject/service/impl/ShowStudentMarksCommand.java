/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.service.impl;

import com.epam.finalproject.controller.ClientType;
import com.epam.finalproject.controller.JspPageName;
import com.epam.finalproject.controller.RequestParameterName;
import com.epam.finalproject.dao.Dao;
import com.epam.finalproject.dao.DaoFactory;
import com.epam.finalproject.dao.impl.DaoException;
import com.epam.finalproject.model.Mark;
import com.epam.finalproject.model.Student;
import com.epam.finalproject.service.CommandException;
import com.epam.finalproject.service.ICommand;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Pavel
 */
public class ShowStudentMarksCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        Student student = null;
        String page = null;
        HttpSession session = request.getSession();
        ClientType clientType = (ClientType) session.getAttribute(RequestParameterName.CLIENT_TYPE);
        switch (clientType) {
            case ADMINISTRATOR:
                try {
                    Dao dao = DaoFactory.getInstance().getDAO(DaoFactory.DAOType.MYSQL);
                    student = dao.getStudentDao().read(Integer.parseInt(request.getParameter(RequestParameterName.ID_STUDENT)));
                    request.setAttribute(RequestParameterName.STUDENT, student);
                    if (dao.getMarkDao().getAllStudentMarks(student.getId()) != null) {
                        List<Mark> marks = dao.getMarkDao().getAllStudentMarks(student.getId());
                        request.setAttribute(RequestParameterName.MARKS, marks);
                    }
                } catch (DaoException e) {
                    throw new CommandException("DaoException in ShowStudentMarksCommand " + e.getMessage(), e);
                }
                page = JspPageName.ADMIN_STUDENT_MARKS;
                break;
            case STUDENT:
                try {
                    Dao dao = DaoFactory.getInstance().getDAO(DaoFactory.DAOType.MYSQL);
                    student = dao.getStudentDao().read((Integer)session.getAttribute(RequestParameterName.ID_STUDENT));
                    request.setAttribute(RequestParameterName.STUDENT, student);
                    if (dao.getMarkDao().getAllStudentMarks(student.getId()) != null) {
                        List<Mark> marks = dao.getMarkDao().getAllStudentMarks(student.getId());
                        request.setAttribute(RequestParameterName.MARKS, marks);
                    }
                } catch (DaoException e) {
                    throw new CommandException("DaoException in ShowStudentMarksCommand " + e.getMessage(), e);
                }
                page = JspPageName.STUDENT_MARKS;
        }
        return page;
    }
}
