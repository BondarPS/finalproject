/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.service.impl;

import com.epam.finalproject.controller.JspPageName;
import com.epam.finalproject.controller.RequestParameterName;
import com.epam.finalproject.dao.Dao;
import com.epam.finalproject.dao.DaoFactory;
import com.epam.finalproject.dao.impl.DaoException;
import com.epam.finalproject.model.Student;
import com.epam.finalproject.model.User;
import com.epam.finalproject.service.CommandException;
import com.epam.finalproject.service.ICommand;
import com.epam.finalproject.util.ParameterValidator;
import com.epam.finalproject.util.PasswordHashCode;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Pavel
 */
public class RegisterCommand implements ICommand {

    private static final String REG_EMAIL = "^[A-Za-z\\_0-9\\.\\-]*[@][A-Za-z\\_0-9\\-]*[.][A-Za-z]{2,3}$";
    private static final String REG_LATIN_AND_CYRILLIC = "(^([А-Яа-я]+)$)|(^([A-Za-z]+)$)";
    private static final String REG_PHONE = "^(375)+[0-9]{9}$";
    private static final String REG_USERNAME = "(^([A-Za-z0-9]+)$)";
    private static final String REG_PASSWORD = "^.{8,16}$";
    private static final String PROP = "prop";
    private static final String INCORRECT_FIRST_NAME_MESSAGE = "prop.incorrect_first_name_message";
    private static final String INCORRECT_LAST_NAME_MESSAGE = "prop.incorrect_last_name_message";
    private static final String INCORRECT_MIDDLE_NAME_MESSAGE = "prop.incorrect_middle_name";
    private static final String INCORRECT_EMAIL_MESSAGE = "prop.incorrect_email_message";
    private static final String INCORRECT_PHONE_MESSAGE = "prop.incorrect_phone_message";
    private static final String INCORRECT_USER_NAME_MESSAGE = "prop.incorrect_user_name_message";
    private static final String INCORRECT_PASSWORD_MESSAGE = "prop.incorrect_password_message";
    private static final String INCORRECT_REPEAT_PASSWORD_MESSAGE = "prop.incorrect_repeat_password_message";
    private static final String INCORRECT_NOT_UNIQUE_MESSAGE = "prop.not_unique_username_message";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        String page = null;
        User user = null;
        Student student = null;
        boolean isFirstNameCorrect = false;
        boolean isLastNameCorrect = false;
        boolean isMiddleNameCorrect = false;
        boolean isEmailCorrect = false;
        boolean isPhoneCorrect = false;
        boolean isUserNameCorrect = false;
        boolean isPasswordCorrect = false;
        boolean isRepeatPasswordCorrect = false;
        boolean isUsernameUnique = false;
        ResourceBundle bundle = null;
        try {
            Dao dao = DaoFactory.getInstance().getDAO(DaoFactory.DAOType.MYSQL);
            List<String> responseMessages = new ArrayList<String>();
            ParameterValidator validator = new ParameterValidator();
            isFirstNameCorrect = validator.checkStringOnMatch(request.getParameter(RequestParameterName.FIRST_NAME), REG_LATIN_AND_CYRILLIC);
            isLastNameCorrect = validator.checkStringOnMatch(request.getParameter(RequestParameterName.LAST_NAME), REG_LATIN_AND_CYRILLIC);
            isMiddleNameCorrect = validator.checkStringOnMatch(request.getParameter(RequestParameterName.MIDDLE_NAME), REG_LATIN_AND_CYRILLIC);
            isEmailCorrect = validator.checkStringOnMatch(request.getParameter(RequestParameterName.EMAIL), REG_EMAIL);
            isPhoneCorrect = validator.checkStringOnMatch(request.getParameter(RequestParameterName.PHONE), REG_PHONE);
            isUserNameCorrect = validator.checkStringOnMatch(request.getParameter(RequestParameterName.USER_NAME), REG_USERNAME);
            isPasswordCorrect = validator.checkStringOnMatch(request.getParameter(RequestParameterName.PASSWORD), REG_PASSWORD);
            HttpSession session = request.getSession();
            if (session.getAttribute(RequestParameterName.LOCALE) == null) {
                session.setAttribute(RequestParameterName.LOCALE, Locale.getDefault().toString());
                bundle = ResourceBundle.getBundle(PROP, Locale.getDefault());
            } else {
                session.setAttribute(RequestParameterName.LOCALE, request.getParameter(RequestParameterName.LOCALE));
                bundle = ResourceBundle.getBundle(PROP, new Locale(request.getParameter(RequestParameterName.LOCALE)));
            }
            if (dao.getUserDao().readByUsername(request.getParameter(RequestParameterName.USER_NAME)) == null) {
                isUsernameUnique = true;
            } else {
                isUsernameUnique = false;
            }
            isRepeatPasswordCorrect = request.getParameter(RequestParameterName.PASSWORD).equals(request.getParameter(RequestParameterName.REPEAT_PASSWORD));
            if (isFirstNameCorrect == false) {
                responseMessages.add(bundle.getString(INCORRECT_FIRST_NAME_MESSAGE));
            }
            if (isLastNameCorrect == false) {
                responseMessages.add(bundle.getString(INCORRECT_LAST_NAME_MESSAGE));
            }
            if (isMiddleNameCorrect == false) {
                responseMessages.add(bundle.getString(INCORRECT_MIDDLE_NAME_MESSAGE));
            }
            if (isEmailCorrect == false) {
                responseMessages.add(bundle.getString(INCORRECT_EMAIL_MESSAGE));
            }
            if (isPhoneCorrect == false) {
                responseMessages.add(bundle.getString(INCORRECT_PHONE_MESSAGE));
            }
            if (isUserNameCorrect == false) {
                responseMessages.add(bundle.getString(INCORRECT_USER_NAME_MESSAGE));
            }
            if (isPasswordCorrect == false) {
                responseMessages.add(bundle.getString(INCORRECT_PASSWORD_MESSAGE));
            }
            if (isRepeatPasswordCorrect == false) {
                responseMessages.add(bundle.getString(INCORRECT_REPEAT_PASSWORD_MESSAGE));
            }
            if (isUsernameUnique == false) {
                responseMessages.add(bundle.getString(INCORRECT_NOT_UNIQUE_MESSAGE));
            }
            if (isFirstNameCorrect && isLastNameCorrect && isMiddleNameCorrect && isUsernameUnique
                    && isEmailCorrect && isPhoneCorrect && isUserNameCorrect && isPasswordCorrect) {
                user = new User();
                user.setIdRole(2);
                user.setPassword(new PasswordHashCode().getHashCode(request.getParameter(RequestParameterName.PASSWORD)));
                user.setUsername(request.getParameter(RequestParameterName.USER_NAME));
                student = new Student();
                student.setFirstName(request.getParameter(RequestParameterName.FIRST_NAME));
                student.setLastName(request.getParameter(RequestParameterName.LAST_NAME));
                student.setMiddleName(request.getParameter(RequestParameterName.MIDDLE_NAME));
                student.setEmail(request.getParameter(RequestParameterName.EMAIL));
                student.setPhoneNumber(request.getParameter(RequestParameterName.PHONE));
                student.setUser(user);
                dao.getStudentDao().create(student);
                student = dao.getStudentDao().read((Integer) session.getAttribute(RequestParameterName.ID_STUDENT));
                request.setAttribute(RequestParameterName.STUDENT, student);
                page = JspPageName.STUDENT_PROFILE;
            } else {
                request.setAttribute(RequestParameterName.MESSAGES, responseMessages);
                page = JspPageName.REGISTER_PAGE;
            }
        } catch (DaoException e) {
            throw new CommandException("DaoException in RegisterCommand " + e.getMessage(), e);
        } catch (NoSuchAlgorithmException e) {
            throw new CommandException("NoSuchAlgorithmException in RegisterCommand " + e.getMessage(), e);
        } catch (UnsupportedEncodingException e) {
            throw new CommandException("UnsupportedEncodingException in RegisterCommand " + e.getMessage(), e);
        }
        return page;
    }
}
