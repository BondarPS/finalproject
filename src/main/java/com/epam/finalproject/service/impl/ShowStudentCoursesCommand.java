/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.service.impl;

import com.epam.finalproject.controller.JspPageName;
import com.epam.finalproject.controller.RequestParameterName;
import com.epam.finalproject.dao.Dao;
import com.epam.finalproject.dao.DaoFactory;
import com.epam.finalproject.dao.impl.DaoException;
import com.epam.finalproject.model.Course;
import com.epam.finalproject.model.Student;
import com.epam.finalproject.service.CommandException;
import com.epam.finalproject.service.ICommand;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Pavel
 */
public class ShowStudentCoursesCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        Student student = null;
        String page = null;
        HttpSession session = request.getSession();
        try {
            Dao dao = DaoFactory.getInstance().getDAO(DaoFactory.DAOType.MYSQL);
            student = dao.getStudentDao().read(Integer.parseInt(request.getParameter(RequestParameterName.ID_STUDENT)));
            request.setAttribute(RequestParameterName.STUDENT, student);
            if (!dao.getCourseDao().getAllStuentCourses(student.getId()).isEmpty()) {
                List<Course> srudentCourses = dao.getCourseDao().getAllStuentCourses(student.getId());
                request.setAttribute(RequestParameterName.STUDENT_COURSES, srudentCourses);
            }
            if (!dao.getCourseDao().getAllNotStuentCourses(student.getId()).isEmpty()) {
                List<Course> courses = dao.getCourseDao().getAllNotStuentCourses(student.getId());
                request.setAttribute(RequestParameterName.COURSES, courses);
            }
        } catch (DaoException e) {
            throw new CommandException("DaoException in ShowCoursesCommand " + e.getMessage(), e);
        }
        return page = JspPageName.ADMIN_STUDENT_COURSES;
    }
}
