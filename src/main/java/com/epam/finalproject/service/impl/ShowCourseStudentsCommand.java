/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.service.impl;

import com.epam.finalproject.controller.JspPageName;
import com.epam.finalproject.controller.RequestParameterName;
import com.epam.finalproject.dao.Dao;
import com.epam.finalproject.dao.DaoFactory;
import com.epam.finalproject.dao.impl.DaoException;
import com.epam.finalproject.model.Course;
import com.epam.finalproject.model.Mark;
import com.epam.finalproject.service.CommandException;
import com.epam.finalproject.service.ICommand;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Pavel
 */
public class ShowCourseStudentsCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        Course course = null;
        String page = null;
        try {
            Dao dao = DaoFactory.getInstance().getDAO(DaoFactory.DAOType.MYSQL);
            course = dao.getCourseDao().read(Integer.parseInt(request.getParameter(RequestParameterName.ID_COURSE)));
            List<Mark> marks = dao.getMarkDao().getAllCourseMarks(course.getId());
            if (!marks.isEmpty()) {
                request.setAttribute(RequestParameterName.MARKS, marks);
            }
        } catch (DaoException e) {
            throw new CommandException("DaoException in ShowCourseStudentsCommand " + e.getMessage(), e);
        }
        page = JspPageName.TEACHER_COURSE_MARKS;
        return page;
    }
}
