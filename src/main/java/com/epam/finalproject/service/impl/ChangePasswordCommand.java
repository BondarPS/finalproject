/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.service.impl;

import com.epam.finalproject.controller.ClientType;
import com.epam.finalproject.controller.JspPageName;
import com.epam.finalproject.controller.RequestParameterName;
import com.epam.finalproject.dao.Dao;
import com.epam.finalproject.dao.DaoFactory;
import com.epam.finalproject.dao.impl.DaoException;
import com.epam.finalproject.model.Student;
import com.epam.finalproject.model.Teacher;
import com.epam.finalproject.model.User;
import com.epam.finalproject.service.CommandException;
import com.epam.finalproject.service.ICommand;
import com.epam.finalproject.util.ParameterValidator;
import com.epam.finalproject.util.PasswordHashCode;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author SBT-Bondar-PS
 */
public class ChangePasswordCommand implements ICommand {

    private static final String REG_PASSWORD = "^.{8,16}$";
    private static final String PROP = "prop";
    private static final String INCORRECT_PASSWORD_MESSAGE = "prop.incorrect_password_message";
    private static final String INCORRECT_REPEAT_PASSWORD_MESSAGE = "prop.incorrect_repeat_password_message";
    private static final String SUCCESSFUL_MESSAGE = "prop.change_password_successful_message";
    

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        User user = null;
        String page = null;
        boolean isPasswordCorrect = false;
        boolean isRepeatPasswordCorrect = false;
        HttpSession session = request.getSession();
        ParameterValidator validator = new ParameterValidator();
        List<String> responseMessages = new ArrayList<String>();
        isPasswordCorrect = validator.checkStringOnMatch(request.getParameter(RequestParameterName.PASSWORD), REG_PASSWORD);
        isRepeatPasswordCorrect = request.getParameter(RequestParameterName.PASSWORD).equals(request.getParameter(RequestParameterName.REPEAT_PASSWORD));
        ClientType clientType = (ClientType) session.getAttribute(RequestParameterName.CLIENT_TYPE);
        String localeName = (String) session.getAttribute(RequestParameterName.LOCALE);
        Locale locale = new Locale(localeName);
        ResourceBundle bundle = ResourceBundle.getBundle(PROP, locale);
        if (isPasswordCorrect == false) {
            responseMessages.add(bundle.getString(INCORRECT_PASSWORD_MESSAGE));
        }
        if (isRepeatPasswordCorrect == false) {
            responseMessages.add(bundle.getString(INCORRECT_REPEAT_PASSWORD_MESSAGE));
        }
        try {
            Dao dao = DaoFactory.getInstance().getDAO(DaoFactory.DAOType.MYSQL);
            switch (clientType) {
                case ADMINISTRATOR:
                    if (request.getParameter(RequestParameterName.ID_TEACHER) != null) {
                        if (isPasswordCorrect == true && isRepeatPasswordCorrect == true) {
                            user = dao.getUserDao().readById(Integer.parseInt(request.getParameter(RequestParameterName.ID_TEACHER)));
                            user.setPassword(new PasswordHashCode().getHashCode(request.getParameter(RequestParameterName.PASSWORD)));
                            dao.getUserDao().update(user);
                            List<Teacher> teachers = dao.getTeacherDao().getAll();
                            request.setAttribute(RequestParameterName.TEACHERS, teachers);
                            request.setAttribute(RequestParameterName.MESSAGE, bundle.getString(SUCCESSFUL_MESSAGE));
                        }
                        page = JspPageName.ADMIN_TEACHERS;
                    } else if (request.getParameter(RequestParameterName.ID_STUDENT) != null) {
                        if (isPasswordCorrect == true && isRepeatPasswordCorrect == true) {
                            user = dao.getUserDao().readById(Integer.parseInt(request.getParameter(RequestParameterName.ID_STUDENT)));
                            user.setPassword(new PasswordHashCode().getHashCode(request.getParameter(RequestParameterName.PASSWORD)));
                            dao.getUserDao().update(user);
                            List<Student> students = dao.getStudentDao().getAll();
                            request.setAttribute(RequestParameterName.STUDENTS, students);
                            request.setAttribute(RequestParameterName.MESSAGE, bundle.getString(SUCCESSFUL_MESSAGE));
                        }
                        page = JspPageName.ADMIN_STUDENTS;
                    }
                    break;
                case STUDENT:
                    if (isPasswordCorrect == true && isRepeatPasswordCorrect == true) {
                        user = dao.getUserDao().readById(Integer.parseInt(request.getParameter(RequestParameterName.ID_STUDENT)));
                        user.setPassword(new PasswordHashCode().getHashCode(request.getParameter(RequestParameterName.PASSWORD)));
                        dao.getUserDao().update(user);
                        Student student = dao.getStudentDao().read(Integer.parseInt(request.getParameter(RequestParameterName.ID_STUDENT)));
                        request.setAttribute(RequestParameterName.STUDENT, student);
                        request.setAttribute(RequestParameterName.MESSAGE, bundle.getString(SUCCESSFUL_MESSAGE));
                    }
                    page = JspPageName.STUDENT_PROFILE;
                    break;
                case TEACHER:
                    if (isPasswordCorrect == true && isRepeatPasswordCorrect == true) {
                        user = dao.getUserDao().readById(Integer.parseInt(request.getParameter(RequestParameterName.ID_TEACHER)));
                        user.setPassword(new PasswordHashCode().getHashCode(request.getParameter(RequestParameterName.PASSWORD)));
                        dao.getUserDao().update(user);
                        Teacher teacher = dao.getTeacherDao().read(Integer.parseInt(request.getParameter(RequestParameterName.ID_TEACHER)));
                        request.setAttribute(RequestParameterName.TEACHER, teacher);
                        request.setAttribute(RequestParameterName.MESSAGE, bundle.getString(SUCCESSFUL_MESSAGE));
                    }
                    page = JspPageName.TEACHER_PROFILE;
            }

        } catch (DaoException e) {
            throw new CommandException("DaoException in ChangePasswordCommand " + e.getMessage(), e);
        } catch (NoSuchAlgorithmException e) {
            throw new CommandException("NoSuchAlgorithmException in ChangePasswordCommand " + e.getMessage(), e);
        } catch (UnsupportedEncodingException e) {
            throw new CommandException("UnsupportedEncodingException in ChangePasswordCommand " + e.getMessage(), e);
        }
        request.setAttribute(RequestParameterName.MESSAGES, responseMessages);
        return page;
    }
}
