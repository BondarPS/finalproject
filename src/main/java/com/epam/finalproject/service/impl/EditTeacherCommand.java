/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.service.impl;

import com.epam.finalproject.controller.JspPageName;
import com.epam.finalproject.controller.RequestParameterName;
import com.epam.finalproject.dao.Dao;
import com.epam.finalproject.dao.DaoFactory;
import com.epam.finalproject.dao.impl.DaoException;
import com.epam.finalproject.model.Teacher;
import com.epam.finalproject.model.User;
import com.epam.finalproject.service.CommandException;
import com.epam.finalproject.service.ICommand;
import com.epam.finalproject.util.ParameterValidator;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Pavel
 */
public class EditTeacherCommand implements ICommand {

    private static final String REG_LATIN_AND_CYRILLIC = "^([А-Яа-я]+)|([A-Za-z]+)$";
    private static final String REG_PHONE = "^(375)[0-9]{9}$";
    private static final String REG_USERNAME = "(^([A-Za-z0-9]+)$)";
    private static final String REG_EMAIL = "^[A-Za-z\\_0-9\\.\\-]*[@][A-Za-z\\_0-9\\-]*[.][A-Za-z]{2,3}$";
    private static final String PROP = "prop";
    private static final String INCORRECT_FIRST_NAME_MESSAGE = "prop.incorrect_first_name_message";
    private static final String INCORRECT_LAST_NAME_MESSAGE = "prop.incorrect_last_name_message";
    private static final String INCORRECT_MIDDLE_NAME_MESSAGE = "prop.incorrect_middle_name";
    private static final String INCORRECT_EMAIL_MESSAGE = "prop.incorrect_email_message";
    private static final String INCORRECT_PHONE_MESSAGE = "prop.incorrect_phone_message";
    private static final String INCORRECT_USER_NAME_MESSAGE = "prop.incorrect_user_name_message";
    private static final String SUCCESSFUL_MESSAGE = "prop.srudent_changed_successful_message";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        User user = null;
        Teacher teacher = null;
        String page = null;
        HttpSession session = request.getSession();
        boolean isfirstNameCorrect = false;
        boolean isLastNameCorrect = false;
        boolean isMiddleNameCorrect = false;
        boolean isEmailCorreсt = false;
        boolean isPhoneNumberCorreсt = false;
        boolean isUsernameCorreсt = false;
        List<String> responseMessages = new ArrayList<String>();
        ParameterValidator validator = new ParameterValidator();
        isfirstNameCorrect = validator.checkStringOnMatch(request.getParameter(RequestParameterName.FIRST_NAME), REG_LATIN_AND_CYRILLIC);
        isLastNameCorrect = validator.checkStringOnMatch(request.getParameter(RequestParameterName.LAST_NAME), REG_LATIN_AND_CYRILLIC);
        isMiddleNameCorrect = validator.checkStringOnMatch(request.getParameter(RequestParameterName.MIDDLE_NAME), REG_LATIN_AND_CYRILLIC);
        isEmailCorreсt = validator.checkStringOnMatch(request.getParameter(RequestParameterName.EMAIL), REG_EMAIL);
        isPhoneNumberCorreсt = validator.checkStringOnMatch(request.getParameter(RequestParameterName.PHONE), REG_PHONE);
        isUsernameCorreсt = validator.checkStringOnMatch(request.getParameter(RequestParameterName.USER_NAME), REG_USERNAME);
        String localeName = (String) session.getAttribute(RequestParameterName.LOCALE);
        Locale locale = new Locale(localeName);
        ResourceBundle bundle = ResourceBundle.getBundle(PROP, locale);
        if (isfirstNameCorrect == false) {
            responseMessages.add(bundle.getString(INCORRECT_FIRST_NAME_MESSAGE));
        }
        if (isLastNameCorrect == false) {
            responseMessages.add(bundle.getString(INCORRECT_LAST_NAME_MESSAGE));
        }
        if (isMiddleNameCorrect == false) {
            responseMessages.add(bundle.getString(INCORRECT_MIDDLE_NAME_MESSAGE));
        }
        if (isEmailCorreсt == false) {
            responseMessages.add(bundle.getString(INCORRECT_EMAIL_MESSAGE));
        }
        if (isPhoneNumberCorreсt == false) {
            responseMessages.add(bundle.getString(INCORRECT_PHONE_MESSAGE));
        }
        if (isUsernameCorreсt == false) {
            responseMessages.add(bundle.getString(INCORRECT_USER_NAME_MESSAGE));
        }

        try {
            Dao dao = DaoFactory.getInstance().getDAO(DaoFactory.DAOType.MYSQL);
            if (isfirstNameCorrect == true && isLastNameCorrect == true && isMiddleNameCorrect == true && isEmailCorreсt == true
                    && isPhoneNumberCorreсt == true && isUsernameCorreсt == true) {
                user = dao.getUserDao().readById(Integer.parseInt(request.getParameter(RequestParameterName.ID_TEACHER)));
                user.setUsername(request.getParameter(RequestParameterName.USER_NAME));
                teacher = dao.getTeacherDao().read(Integer.parseInt(request.getParameter(RequestParameterName.ID_TEACHER)));
                teacher.setFirstName(request.getParameter(RequestParameterName.FIRST_NAME));
                teacher.setLastName(request.getParameter(RequestParameterName.LAST_NAME));
                teacher.setMiddleName(request.getParameter(RequestParameterName.MIDDLE_NAME));
                teacher.setPhoneNumber(request.getParameter(RequestParameterName.PHONE));
                teacher.setEmail(request.getParameter(RequestParameterName.EMAIL));
                user = new User();
                user.setUsername(request.getParameter(RequestParameterName.USER_NAME));
                user.setId(Integer.parseInt(request.getParameter(RequestParameterName.ID_TEACHER)));
                teacher.setUser(user);
                dao.getTeacherDao().update(teacher);
                List<Teacher> teachers = dao.getTeacherDao().getAll();
                request.setAttribute(RequestParameterName.TEACHERS, teachers);
                request.setAttribute(RequestParameterName.MESSAGE, bundle.getString(SUCCESSFUL_MESSAGE));
            } else {
                List<Teacher> teachers = dao.getTeacherDao().getAll();
                request.setAttribute(RequestParameterName.TEACHERS, teachers);
                request.setAttribute(RequestParameterName.MESSAGES, responseMessages);
            }
        } catch (DaoException e) {
            throw new CommandException("DaoException in EditTeacherCommand " + e.getMessage(), e);
        }
        page = JspPageName.ADMIN_TEACHERS;
        return page;
    }
}
