/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.service.impl;

import com.epam.finalproject.controller.ClientType;
import com.epam.finalproject.controller.JspPageName;
import com.epam.finalproject.controller.RequestParameterName;
import com.epam.finalproject.dao.Dao;
import com.epam.finalproject.dao.DaoFactory;
import com.epam.finalproject.dao.impl.DaoException;
import com.epam.finalproject.model.Course;
import com.epam.finalproject.model.Role;
import com.epam.finalproject.model.Student;
import com.epam.finalproject.model.Teacher;
import com.epam.finalproject.service.CommandException;
import com.epam.finalproject.service.ICommand;
import com.epam.finalproject.model.User;
import com.epam.finalproject.util.PasswordHashCode;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Pavel
 */
public class LoginCommand implements ICommand {

    private static final String PROP = "prop";
    private static final String SUCCESSFUL_MESSAGE = "prop.srudent_changed_successful_message";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = null;
        User user = null;
        Role role = null;
        Student student = null;
        Teacher teacher = null;
        ClientType clientType = null;
        ResourceBundle bundle = null;
        HttpSession session = request.getSession();
        if (session.getAttribute(RequestParameterName.LOCALE) == null) {
            session.setAttribute(RequestParameterName.LOCALE, request.getParameter(RequestParameterName.LOCALE));
            bundle = ResourceBundle.getBundle(PROP, new Locale((String)session.getAttribute(RequestParameterName.LOCALE)));
        }
        try {
            Dao dao = DaoFactory.getInstance().getDAO(DaoFactory.DAOType.MYSQL);
            user = dao.getUserDao().readByUsername(request.getParameter(RequestParameterName.USER_NAME));
            if (user != null) {
                try {
                    if (new PasswordHashCode().getHashCode(request.getParameter(RequestParameterName.PASSWORD)).equals(user.getPassword())) {
                        role = dao.getRoleDao().read(user.getIdRole());
                        clientType = ClientType.valueOf(role.getName().toUpperCase());
                        switch (clientType) {
                            case ADMINISTRATOR:
                                List<Course> courses = dao.getCourseDao().getAll();
                                request.setAttribute(RequestParameterName.COURSES, courses);
                                page = JspPageName.ADMIN_COURSES;
                                break;
                            case STUDENT:
                                student = dao.getStudentDao().read(user.getId());
                                request.setAttribute(RequestParameterName.STUDENT, student);
                                session.setAttribute(RequestParameterName.ID_STUDENT, student.getId());
                                page = JspPageName.STUDENT_PROFILE;
                                break;
                            case TEACHER:
                                teacher = dao.getTeacherDao().read(user.getId());
                                request.setAttribute(RequestParameterName.TEACHER, teacher);
                                session.setAttribute(RequestParameterName.ID_TEACHER, teacher.getId());
                                page = JspPageName.TEACHER_PROFILE;
                                break;
                            default:
                                page = JspPageName.LOGIN_PAGE;
                                break;
                        }
                        session.setAttribute(RequestParameterName.CLIENT_TYPE, clientType);
                        session.setAttribute(RequestParameterName.USER_NAME, request.getParameter(RequestParameterName.USER_NAME));
                    } else {
                        request.setAttribute(RequestParameterName.MESSAGE, bundle.getString("prop.incorrect_login_or_password_message"));
                        page = JspPageName.LOGIN_PAGE;
                    }
                } catch (NoSuchAlgorithmException e) {
                    throw new CommandException("NoSuchAlgorithmException in LoginCommand \n", e);
                } catch (UnsupportedEncodingException e) {
                    throw new CommandException("UnsupportedEncodingException in LoginCommand \n", e);
                }
            } else {
                request.setAttribute(RequestParameterName.MESSAGE, bundle.getString("prop.incorrect_login_or_password_message"));
                page = JspPageName.LOGIN_PAGE;
            }
        } catch (DaoException e) {
            throw new CommandException("DaoException in LoginCommand \n", e);
        }
        return page;
    }
}
