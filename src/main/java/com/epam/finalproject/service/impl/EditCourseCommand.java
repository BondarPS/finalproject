/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.service.impl;

import com.epam.finalproject.controller.ClientType;
import com.epam.finalproject.controller.JspPageName;
import com.epam.finalproject.controller.RequestParameterName;
import com.epam.finalproject.dao.Dao;
import com.epam.finalproject.dao.DaoFactory;
import com.epam.finalproject.dao.impl.DaoException;
import com.epam.finalproject.model.Course;
import com.epam.finalproject.service.CommandException;
import com.epam.finalproject.service.ICommand;
import com.epam.finalproject.util.ParameterValidator;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Pavel
 */
public class EditCourseCommand implements ICommand {

    private static final String REG_DATE = "^([0-9]{4}-[0-9]{2}-[0-9]{2})$";
    private static final String REG_COURSE_NAME = "^([А-Яа-яA-Za-z0-9\\+\\-\\_ ]+)$";
    private static final String REG_ENDED = "^([Дд]{1}а)|([Нн]{1}ет)$";
    private static final String DATE_FORMAT = "yyyy-MM-dd";
    private static final String PROP = "prop";
    private static final String INCORRECT_COURSE_NAME_MESSAGE = "prop.incorrect_course_name_message";
    private static final String INCORRECT_DATE_MESSAGE = "prop.incorrect_date_message";
    private static final String INCORRECT_ENDED_MESSAGE = "prop.incorrect_ended_message";
    private static final String IS_ENDED = "prop.is_ended";
    private static final String SUCCESSFUL_MESSAGE = "prop.course_changed_succesful_message";
    

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        Dao dao = null;
        Course course = null;
        String page = null;
        boolean isStartDateCorrect = false;
        boolean isEndDateCorrect = false;
        boolean isCourseNameCorrect = false;
        boolean isEndedCorrect = false;
        List<String> responseMessages = new ArrayList<String>();
        ParameterValidator validator = new ParameterValidator();
        isStartDateCorrect = validator.checkStringOnMatch(request.getParameter(RequestParameterName.START_DATE), REG_DATE);
        isEndDateCorrect = validator.checkStringOnMatch(request.getParameter(RequestParameterName.END_DATE), REG_DATE);
        isCourseNameCorrect = validator.checkStringOnMatch(request.getParameter(RequestParameterName.COURSE_NAME), REG_COURSE_NAME);
        isEndedCorrect = validator.checkStringOnMatch(request.getParameter(RequestParameterName.IS_ENDED), REG_ENDED);
        HttpSession session = request.getSession();
        ClientType clientType = (ClientType) session.getAttribute(RequestParameterName.CLIENT_TYPE);
        String localeName = (String) session.getAttribute(RequestParameterName.LOCALE);
        Locale locale = new Locale(localeName);
        ResourceBundle bundle = ResourceBundle.getBundle(PROP, locale);
        if (isCourseNameCorrect == false) {
            responseMessages.add(bundle.getString(INCORRECT_COURSE_NAME_MESSAGE));
        }
        if (isStartDateCorrect == false) {
            responseMessages.add(bundle.getString(INCORRECT_DATE_MESSAGE));
        }
        if (isEndDateCorrect == false) {
            responseMessages.add(bundle.getString(INCORRECT_DATE_MESSAGE));
        }
        if (isEndedCorrect == false) {
            responseMessages.add(bundle.getString(INCORRECT_ENDED_MESSAGE));
        }
        try {
            dao = DaoFactory.getInstance().getDAO(DaoFactory.DAOType.MYSQL);
            if (isCourseNameCorrect == true && isStartDateCorrect == true && isEndDateCorrect == true && isEndedCorrect == true) {
                course = dao.getCourseDao().readWithTeacher(Integer.parseInt(request.getParameter(RequestParameterName.ID_COURSE)));
                course.setCourseName(request.getParameter(RequestParameterName.COURSE_NAME));
                SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
                Date date = formatter.parse(request.getParameter(RequestParameterName.START_DATE));
                course.setStartDate(date);
                date = formatter.parse(request.getParameter(RequestParameterName.END_DATE));
                course.setEndDate(date);
                if (RequestParameterName.IS_ENDED.toLowerCase().equals(bundle.getString(IS_ENDED))) {
                    course.setEnded(true);
                } else {
                    course.setEnded(false);
                }
                dao.getCourseDao().update(course);
                List<Course> courses = dao.getCourseDao().getAll();
                request.setAttribute(RequestParameterName.COURSES, courses);
                request.setAttribute(RequestParameterName.MESSAGE, bundle.getString(SUCCESSFUL_MESSAGE));
            } else {
                List<Course> courses = dao.getCourseDao().getAll();
                request.setAttribute(RequestParameterName.COURSES, courses);
                request.setAttribute(RequestParameterName.MESSAGES, responseMessages);
            }
        } catch (DaoException e) {
            throw new CommandException("DaoException in EditCourseCommand " + e.getMessage(), e);
        } catch (ParseException e) {
            throw new CommandException("ParseException in EditCourseCommand " + e.getMessage(), e);
        }

        page = JspPageName.ADMIN_COURSES;
        return page;
    }
}
