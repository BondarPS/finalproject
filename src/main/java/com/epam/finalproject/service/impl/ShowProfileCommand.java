/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.service.impl;

import com.epam.finalproject.controller.ClientType;
import com.epam.finalproject.controller.JspPageName;
import com.epam.finalproject.controller.RequestParameterName;
import com.epam.finalproject.dao.Dao;
import com.epam.finalproject.dao.DaoFactory;
import com.epam.finalproject.dao.impl.DaoException;
import com.epam.finalproject.model.Student;
import com.epam.finalproject.model.Teacher;
import com.epam.finalproject.service.CommandException;
import com.epam.finalproject.service.ICommand;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Pavel
 */
public class ShowProfileCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = null;
        Teacher teacher = null;
        Student student = null;
        ClientType clientType = null;
        HttpSession session = request.getSession();
        try {
            Dao dao = DaoFactory.getInstance().getDAO(DaoFactory.DAOType.MYSQL);
            clientType = (ClientType) session.getAttribute(RequestParameterName.CLIENT_TYPE);            
            switch (clientType) {
                case STUDENT:
                    student = dao.getStudentDao().read((Integer) session.getAttribute(RequestParameterName.ID_STUDENT));
                    request.setAttribute(RequestParameterName.STUDENT, student);
                    page = JspPageName.STUDENT_PROFILE;
                    break;
                case TEACHER:
                    teacher = dao.getTeacherDao().read((Integer) session.getAttribute(RequestParameterName.ID_TEACHER));
                    request.setAttribute(RequestParameterName.TEACHER, teacher);
                    page = JspPageName.TEACHER_PROFILE;
                    break;
                default:
                    page = JspPageName.LOGIN_PAGE;
                    break;
            }
        } catch (DaoException e) {
            throw new CommandException("DaoException in ShowProfileCommand " + e.getMessage(), e);
        }
        return page;
    }
}
