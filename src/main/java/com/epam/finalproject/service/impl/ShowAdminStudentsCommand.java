/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.epam.finalproject.service.impl;

import com.epam.finalproject.controller.JspPageName;
import com.epam.finalproject.controller.RequestParameterName;
import com.epam.finalproject.dao.Dao;
import com.epam.finalproject.dao.DaoFactory;
import com.epam.finalproject.dao.impl.DaoException;
import com.epam.finalproject.model.Student;
import com.epam.finalproject.service.CommandException;
import com.epam.finalproject.service.ICommand;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Pavel
 */
public class ShowAdminStudentsCommand implements ICommand{
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = null;
        try {
            Dao dao = DaoFactory.getInstance().getDAO(DaoFactory.DAOType.MYSQL);
            List<Student> students = dao.getStudentDao().getAll();
            request.setAttribute(RequestParameterName.STUDENTS, students);
        } catch (DaoException e) {
            throw new CommandException("DaoException in ShowAdminStudents" +e.getMessage(), e);
        }
        page = JspPageName.ADMIN_STUDENTS;
        return page;
    }
}
