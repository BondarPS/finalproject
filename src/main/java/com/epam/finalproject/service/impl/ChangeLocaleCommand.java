/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.service.impl;

import com.epam.finalproject.controller.JspPageName;
import com.epam.finalproject.controller.RequestParameterName;
import com.epam.finalproject.service.CommandException;
import com.epam.finalproject.service.ICommand;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Pavel
 */
public class ChangeLocaleCommand implements ICommand {

    private static final String LOGIN_PAGE = "login_page";
    private static final String REGISTER_PAGE = "register_page";
    
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = null;
        HttpSession session = request.getSession();
        session.setAttribute(RequestParameterName.LOCALE, request.getParameter(RequestParameterName.LOCALE));
        switch (request.getParameter(RequestParameterName.PAGE)) {
            case LOGIN_PAGE:
                page = JspPageName.LOGIN_PAGE;
                break;
            case REGISTER_PAGE:
                page = JspPageName.REGISTER_PAGE;
                break;
            default:
                page = JspPageName.LOGIN_PAGE;
        }
        return page;
    }

}
