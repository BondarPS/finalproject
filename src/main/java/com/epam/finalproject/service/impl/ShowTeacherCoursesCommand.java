/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.epam.finalproject.service.impl;

import com.epam.finalproject.controller.JspPageName;
import com.epam.finalproject.controller.RequestParameterName;
import com.epam.finalproject.dao.Dao;
import com.epam.finalproject.dao.DaoFactory;
import com.epam.finalproject.dao.impl.DaoException;
import com.epam.finalproject.model.Course;
import com.epam.finalproject.model.Teacher;
import com.epam.finalproject.service.CommandException;
import com.epam.finalproject.service.ICommand;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Pavel
 */
public class ShowTeacherCoursesCommand implements ICommand{
    
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        Teacher teacher = null;
        String page = null;
        try {
            Dao dao = DaoFactory.getInstance().getDAO(DaoFactory.DAOType.MYSQL);
            teacher = dao.getTeacherDao().read(Integer.parseInt(request.getParameter(RequestParameterName.ID_TEACHER)));
            request.setAttribute(RequestParameterName.TEACHER, teacher);
            List<Course> courses = dao.getCourseDao().getAllTeacherCourses(teacher.getId());
            request.setAttribute(RequestParameterName.COURSES, courses);
        } catch (DaoException e) {
            throw new CommandException("DaoException in ShowTeacherCourseCommand " + e.getMessage(), e);
        }
        page = JspPageName.ADMIN_TEACHER_COURSES;
        return page;
    }
}
