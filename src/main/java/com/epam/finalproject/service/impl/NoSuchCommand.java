/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.service.impl;

import com.epam.finalproject.controller.JspPageName;
import com.epam.finalproject.service.CommandException;
import com.epam.finalproject.service.ICommand;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Pavel
 */
public class NoSuchCommand implements ICommand{

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        return JspPageName.ERROR_PAGE;
    }

}
