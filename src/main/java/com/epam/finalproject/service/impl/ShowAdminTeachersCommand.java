/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.service.impl;

import com.epam.finalproject.controller.JspPageName;
import com.epam.finalproject.controller.RequestParameterName;
import com.epam.finalproject.dao.Dao;
import com.epam.finalproject.dao.DaoFactory;
import com.epam.finalproject.dao.impl.DaoException;
import com.epam.finalproject.model.Teacher;
import com.epam.finalproject.service.CommandException;
import com.epam.finalproject.service.ICommand;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Pavel
 */
public class ShowAdminTeachersCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = null;
        try {
            Dao dao = DaoFactory.getInstance().getDAO(DaoFactory.DAOType.MYSQL);
            List<Teacher> teachers = dao.getTeacherDao().getAll();
            request.setAttribute(RequestParameterName.TEACHERS, teachers);
        } catch (DaoException e) {
            throw new CommandException("DaoException in ShowAdminTeachers " + e.getMessage(), e);
        }
        page = JspPageName.ADMIN_TEACHERS;
        return page;
    }
}
