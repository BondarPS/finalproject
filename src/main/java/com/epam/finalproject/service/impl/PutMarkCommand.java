/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.service.impl;

import com.epam.finalproject.controller.JspPageName;
import com.epam.finalproject.controller.RequestParameterName;
import com.epam.finalproject.dao.Dao;
import com.epam.finalproject.dao.DaoFactory;
import com.epam.finalproject.dao.impl.DaoException;
import com.epam.finalproject.model.Mark;
import com.epam.finalproject.service.CommandException;
import com.epam.finalproject.service.ICommand;
import com.epam.finalproject.util.ParameterValidator;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author SBT-Bondar-PS
 */
public class PutMarkCommand implements ICommand {

    private static final String REG_MARK = "^([1-9]|10){1}$";
    private static final String REG_COMMENTS = "^[A-Za-zА-Яа-я0-9\\s\\?\\.\\!\\_\\-\\+\\,]{1,255}$";
    private static final String PROP = "prop";
    private static final String INCORRECT_MARK_MESSAGE = "prop.incorrect_mark_message";
    private static final String INCORRECT_COMMENTS_MESSAGE = "prop.incorrect_comments_message";
    private static final String SUCCESSFUL_MESSAGE = "prop.mark_added_successful_message";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = null;
        HttpSession session = request.getSession();
        Dao dao = null;
        Mark mark = null;
        boolean isMarkCorrect = false;
        boolean isCommentsCorrect = false;
        List<String> responseMessages = new ArrayList<String>();
        ParameterValidator validator = new ParameterValidator();
        isMarkCorrect = validator.checkStringOnMatch(request.getParameter(RequestParameterName.MARK), REG_MARK);
        isCommentsCorrect = validator.checkStringOnMatch(request.getParameter(RequestParameterName.COMMENTS), REG_COMMENTS);
        String localeName = (String) session.getAttribute(RequestParameterName.LOCALE);
        Locale locale = new Locale(localeName);
        ResourceBundle bundle = ResourceBundle.getBundle(PROP, locale);
        if (isMarkCorrect == false) {
            responseMessages.add(bundle.getString(INCORRECT_MARK_MESSAGE));
        }
        if (isCommentsCorrect == false) {
            responseMessages.add(bundle.getString(INCORRECT_COMMENTS_MESSAGE));
        }
        try {
            dao = DaoFactory.getInstance().getDAO(DaoFactory.DAOType.MYSQL);
            if (isMarkCorrect == true && isCommentsCorrect == true) {
                mark = dao.getMarkDao().read(Integer.parseInt(request.getParameter(RequestParameterName.ID_MARK)));
                mark.setMark(Integer.parseInt(request.getParameter(RequestParameterName.MARK)));
                mark.setComments(request.getParameter(RequestParameterName.COMMENTS));
                dao.getMarkDao().update(mark);
                List<Mark> marks = dao.getMarkDao().getAllCourseMarks(mark.getCourse().getId());
                if (marks.size() != 0) {
                    request.setAttribute(RequestParameterName.MARKS, marks);
                }
                request.setAttribute(RequestParameterName.MESSAGE, bundle.getString(SUCCESSFUL_MESSAGE));
            }
        } catch (DaoException e) {
            throw new CommandException("DaoException in PutMarkCommand " + e.getMessage(), e);
        }
        request.setAttribute(RequestParameterName.MESSAGES, responseMessages);
        page = JspPageName.TEACHER_COURSE_MARKS;
        return page;
    }
}
