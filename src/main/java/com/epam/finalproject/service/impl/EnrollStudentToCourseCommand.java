/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.service.impl;

import com.epam.finalproject.controller.JspPageName;
import com.epam.finalproject.controller.RequestParameterName;
import com.epam.finalproject.dao.Dao;
import com.epam.finalproject.dao.DaoFactory;
import com.epam.finalproject.dao.impl.DaoException;
import com.epam.finalproject.model.Course;
import com.epam.finalproject.model.Mark;
import com.epam.finalproject.model.Student;
import com.epam.finalproject.model.StudentCourse;
import com.epam.finalproject.service.CommandException;
import com.epam.finalproject.service.ICommand;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Pavel
 */
public class EnrollStudentToCourseCommand implements ICommand {

    private static final String PROP = "prop";
    private static final String SUCCESSFUL_MESSAGE = "prop.student_enrolled_succesful_message";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = null;
        StudentCourse studentCourse = null;
        Mark mark = null;
        Student student = null;
        Course course = null;
        HttpSession session = request.getSession();
        String localeName = (String) session.getAttribute(RequestParameterName.LOCALE);
        Locale locale = new Locale(localeName);
        ResourceBundle bundle = ResourceBundle.getBundle(PROP, locale);
        try {
            studentCourse = new StudentCourse();
            studentCourse.setIdCourse(Integer.parseInt(request.getParameter(RequestParameterName.ID_COURSE)));
            studentCourse.setIdStudent((Integer) session.getAttribute(RequestParameterName.ID_STUDENT));
            Dao dao = DaoFactory.getInstance().getDAO(DaoFactory.DAOType.MYSQL);
            course = dao.getCourseDao().read(Integer.parseInt(request.getParameter(RequestParameterName.ID_COURSE)));
            student = dao.getStudentDao().read((Integer) session.getAttribute(RequestParameterName.ID_STUDENT));
            mark = new Mark();
            mark.setCourse(course);
            mark.setStudent(student);
            dao.getStudentCourseDao().create(studentCourse);
            dao.getMarkDao().create(mark);
            if (!dao.getCourseDao().getAllStuentCourses((Integer) session.getAttribute(RequestParameterName.ID_STUDENT)).isEmpty()) {
                List<Course> srudentCourses = dao.getCourseDao().getAllStuentCourses((Integer) session.getAttribute(RequestParameterName.ID_STUDENT));
                request.setAttribute(RequestParameterName.STUDENT_COURSES, srudentCourses);
            }
            if (!dao.getCourseDao().getAllNotStuentCourses((Integer) session.getAttribute(RequestParameterName.ID_STUDENT)).isEmpty()) {
                List<Course> courses = dao.getCourseDao().getAllNotStuentCourses((Integer) session.getAttribute(RequestParameterName.ID_STUDENT));
                request.setAttribute(RequestParameterName.COURSES, courses);
            }
            request.setAttribute(RequestParameterName.MESSAGE, bundle.getString(SUCCESSFUL_MESSAGE));
            page = JspPageName.STUDENT_COURSES;

        } catch (DaoException e) {
            throw new CommandException("DaoException in EnrollStudentToCourseCommand " + e.getMessage(), e);
        }
        return page;
    }
}
