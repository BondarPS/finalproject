/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.service.impl;

import com.epam.finalproject.controller.JspPageName;
import com.epam.finalproject.controller.RequestParameterName;
import com.epam.finalproject.dao.Dao;
import com.epam.finalproject.dao.DaoFactory;
import com.epam.finalproject.dao.impl.DaoException;
import com.epam.finalproject.model.Course;
import com.epam.finalproject.model.Student;
import com.epam.finalproject.model.Teacher;
import com.epam.finalproject.service.CommandException;
import com.epam.finalproject.service.ICommand;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Pavel
 */
public class AppointTeacherCommand implements ICommand {

    private static final String PROP = "prop";
    private static final String SUCCESSFUL_MESSAGE = "prop.teacher_successful_appointed";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        Course course = null;
        Teacher teacher = null;
        String page = null;
        HttpSession session = request.getSession();
        String localeName = (String) session.getAttribute(RequestParameterName.LOCALE);
        Locale locale = new Locale(localeName);
        ResourceBundle bundle = ResourceBundle.getBundle(PROP, locale);
        try {
            Dao dao = DaoFactory.getInstance().getDAO(DaoFactory.DAOType.MYSQL);
            course = dao.getCourseDao().read(Integer.parseInt(request.getParameter(RequestParameterName.ID_COURSE)));
            teacher = dao.getTeacherDao().read(Integer.parseInt(request.getParameter(RequestParameterName.ID_TEACHER)));
            course.setTeacher(teacher);
            dao.getCourseDao().update(course);
            request.setAttribute(RequestParameterName.COURSE, course);
            if (course.getTeacher() != null) {
                teacher = course.getTeacher();
                request.setAttribute(RequestParameterName.TEACHER, teacher);
            } else {
                List<Teacher> teachers;
                teachers = dao.getTeacherDao().getAll();
                request.setAttribute(RequestParameterName.TEACHERS, teachers);
            }
            List<Student> students = dao.getStudentDao().getAllStudentsByCourse(course.getId());
            request.setAttribute(RequestParameterName.STUDENTS, students);
            request.setAttribute(RequestParameterName.MESSAGE, bundle.getString(SUCCESSFUL_MESSAGE));
        } catch (DaoException e) {
            throw new CommandException("DaoException in AppointTeacherCommand " + e.getMessage(), e);
        }
        page = JspPageName.ADMIN_COURSE_DETAIL;
        return page;
    }
}
