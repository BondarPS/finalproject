/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.service;

/**
 *
 * @author Pavel
 */
public enum CommandName {

    LOGIN, LOGOUT,CHANGE_LOCALE, NO_SUCH_COMMAND, REGISTER, SHOW_ADMIN_STUDENTS, SHOW_ADMIN_TEACHERS,
    ADD_COURSE, ADD_TEACHER, EDIT_COURSE, EDIT_TEACHER, CHANGE_PASSWORD, SHOW_DETAIL_COURSE_INFORMATION, APPOINT_TEACHER,
    SHOW_TEACHER_COURSES, EDIT_STUDENT, SHOW_COURSES, SHOW_STUDENT_MARKS, SHOW_PROFILE, ENROLL_STUDENT_TO_COURSE, SHOW_COURSE_STUDENTS,
    PUT_MARK, SHOW_STUDENT_COURSES
}
