/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.dao;

import com.epam.finalproject.dao.impl.DaoException;
import com.epam.finalproject.model.Student;
import java.util.List;

/**
 *
 * @author Pavel
 */
public interface StudentDao {

    public void create(Student student) throws DaoException;

    public Student read(int idStudent) throws DaoException;

    public void update(Student student) throws DaoException;

    public void delete(Student student) throws DaoException;

    public List<Student> getAll() throws DaoException;
    
    public List<Student> getAllStudentsByCourse(int idCourse) throws DaoException;
    
}
