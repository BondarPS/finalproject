/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.dao;

import com.epam.finalproject.dao.impl.DaoException;
import com.epam.finalproject.model.Teacher;
import java.util.List;

/**
 *
 * @author Pavel
 */
public interface TeacherDao {

    public void create(Teacher teacher) throws DaoException;

    public Teacher read(int idTeacher) throws DaoException;

    public void update(Teacher teacher) throws DaoException;

    public List<Teacher> getAll() throws DaoException;
}
