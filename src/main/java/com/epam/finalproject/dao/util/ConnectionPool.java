/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.dao.util;

/**
 *
 * @author Pavel
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class ConnectionPool {

    private static ConnectionPool instance;
    private BlockingQueue<Connection> availableConnections;
    private AtomicInteger usedConnectionsCount;
    private static final String URL = "jdbc:mysql://localhost:3306/electives?encoding=UTF-8&useUnicode=true&characterEncoding=UTF-8"; //URL адрес 
    private static final String USER = "root";//Логин пользователя private 
    private static final String PASSWORD = "ghjjcnjj";//Пароль пользователя private 
    private static final String DRIVER = "com.mysql.jdbc.Driver";//Имя драйвера 
    private static final int INIT_CONNECTIONS_COUNT = 10;

    private ConnectionPool() throws ConnectionPoolException {
        availableConnections = new ArrayBlockingQueue<Connection>(INIT_CONNECTIONS_COUNT);
        usedConnectionsCount = new AtomicInteger(0);
        try {
            Class.forName(DRIVER);
        } catch (ClassNotFoundException e) {
            throw new ConnectionPoolException("ClassNotFoundException during ConnectionPool intilization", e);
        } catch (Exception e) {
            throw new ConnectionPoolException("Exception during ConnectionPool intilization", e);
        }
        for (int i = 0; i < INIT_CONNECTIONS_COUNT; i++) {
            availableConnections.add(createConnection(URL, USER, PASSWORD));
        }
    }

    public static synchronized ConnectionPool getInstance() throws ConnectionPoolException {
        if (instance== null) {
            instance = new ConnectionPool(); 
        }
        return instance;
    }

    private Connection createConnection(String url, String user, String password) throws ConnectionPoolException {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, user, password);
        } catch (Exception e) {
            throw new ConnectionPoolException("Exception during creating Connection", e);
        }
        return conn;
    }

    public Connection retrieve() throws ConnectionPoolException {
        Connection newConnection = null;
        try {
            if (availableConnections.size() != 0) {
                newConnection = availableConnections.take();
                usedConnectionsCount.getAndIncrement();
            } else {
                throw new ConnectionPoolException("ConnectionPool is empty. All connections is busy");
            }
        } catch (InterruptedException e) {
            throw new ConnectionPoolException("InterruptedException generated in ConnectionPool", e);
        }
        return newConnection;
    }

    public void putback(Connection c) throws ConnectionPoolException {
        try {
            if (c != null) {
                if (usedConnectionsCount.intValue() != 0) {
                    availableConnections.put(c);
                    usedConnectionsCount.getAndDecrement();
                }
            }
        } catch (InterruptedException e) {
            throw new ConnectionPoolException("InterruptedException generated in ConnectionPool", e);
        }
    }

    public int getAvailableConnectionsCount() {
        return availableConnections.size();
    }
}
