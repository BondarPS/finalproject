/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.dao.util;

/**
 *
 * @author Pavel
 */
public class ConnectionPoolException extends Exception {

    private static final long serialVersionUID = 1L;

    public ConnectionPoolException(String msg) {
        super(msg);
    }

    public ConnectionPoolException(String msg, Exception e) {
        super(msg, e);
    }
}
