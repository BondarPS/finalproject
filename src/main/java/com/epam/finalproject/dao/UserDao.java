/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.dao;

import com.epam.finalproject.dao.impl.DaoException;
import com.epam.finalproject.model.User;
import java.util.List;

/**
 *
 * @author Pavel
 */
public interface UserDao {

    public void create(User user) throws DaoException;

    public User readByUsername(String username) throws DaoException;
    
    public User readById(int id) throws DaoException;

    public void update(User user) throws DaoException;

    public void delete(User user) throws DaoException;

    public List<User> getAll() throws DaoException;
}
