/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.dao;

import com.epam.finalproject.dao.impl.DaoException;
import com.epam.finalproject.model.Course;
import java.util.List;

/**
 *
 * @author Pavel
 */
public interface CourseDao {

    public void create(Course course) throws DaoException;

    public Course read(int idCourse) throws DaoException;

    public Course readWithTeacher(int idCourse) throws DaoException;

    public void update(Course course) throws DaoException;

    public List<Course> getAll() throws DaoException;

    public List<Course> getAllTeacherCourses(int idTeacher) throws DaoException;
    
    public List<Course> getAllStuentCourses(int idStudent) throws DaoException;
    
    public List<Course> getAllNotStuentCourses(int idStudent) throws DaoException;
}
