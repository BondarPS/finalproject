/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.epam.finalproject.dao;

import com.epam.finalproject.dao.impl.DaoException;
import com.epam.finalproject.model.Role;
import java.util.List;

/**
 *
 * @author Pavel
 */
public interface RoleDao {
    
    public Role read(int userId) throws DaoException;

    public List<Role> getAll() throws DaoException;
}
