/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.epam.finalproject.dao;

import com.epam.finalproject.dao.impl.DaoException;
import com.epam.finalproject.model.StudentCourse;


/**
 *
 * @author Pavel
 */
public interface StudentCourseDao {
    public void create(StudentCourse studentCourse) throws DaoException;
    public void delete (StudentCourse studentCourse) throws DaoException;
}
