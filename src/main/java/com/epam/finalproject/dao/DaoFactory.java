/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.epam.finalproject.dao;

import com.epam.finalproject.dao.impl.DaoException;
import com.epam.finalproject.dao.impl.MysqlDao;

/**
 *
 * @author Pavel
 */
public class DaoFactory {
    private final static DaoFactory instance = new DaoFactory();

    public static DaoFactory getInstance() {
        return instance;
    }

    public Dao getDAO(DAOType type) throws DaoException {
        Dao dao;
        switch (type) {
            case MYSQL:
                return MysqlDao.getInstance();
            default:
                throw new DaoException("No such DAO");
        }
    }

    public enum DAOType {
        MYSQL;
    }
}
