/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.dao.impl;

import com.epam.finalproject.dao.StudentDao;
import com.epam.finalproject.dao.util.ConnectionPool;
import com.epam.finalproject.dao.util.ConnectionPoolException;
import com.epam.finalproject.model.Student;
import com.epam.finalproject.model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Pavel
 */
public class DbStudentDao implements StudentDao {

    private final static String SELECT_STUDENT = "SELECT st.idStudent, st.firstName, st.lastName, st.middleName, st.email, st.phoneNumber, u.username "
            + "FROM electives.student st inner join electives.user u on st.idStudent = u.idUser WHERE idStudent = ?;";
    private final static String SELECT_STUDENTS = "SELECT st.idStudent, st.firstName, st.lastName, st.middleName, st.email, st.phoneNumber, u.username "
            + "FROM electives.student st inner join electives.user u on st.idStudent = u.idUser;";
    private final static String SELECT_STUDENTS_BY_COURSE = "SELECT st.idStudent, st.firstName, st.lastName, st.middleName, st.email, st.phoneNumber, u.username "
            + "FROM electives.student st inner join studentcourse sc on st.idStudent = sc.idStudent "
            + "inner join user u on st.idStudent = u.idUser where sc.idCourse = ?;";
    private final static String INSERT_STUDENT = "INSERT INTO electives.student (firstName, lastName, middleName, email, phoneNumber, idStudent)"
            + "  VALUES (?, ?, ?, ?, ?, ?);";
    private final static String INSERT_USER = "INSERT INTO electives.user (username, password, idRole)  VALUES (?, ?, ?);";
    private final static String UPDATE_STUDENT = "UPDATE electives.student SET "
            + "firstName = ?, lastName = ?, middleName = ?, email = ?, phoneNumber = ? WHERE idStudent = ?;";
    private final static String UPDATE_USER = "UPDATE electives.user SET "
            + "username = ? WHERE idUser = ?;";
    private final static String DELETE_STUDENT = "DELETE FROM electives.student where idStudent = ?;";


    public DbStudentDao() {

    }

    @Override
    public void create(Student student) throws DaoException {
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            connection.setAutoCommit(false);
            PreparedStatement stm = connection.prepareStatement(INSERT_USER, PreparedStatement.RETURN_GENERATED_KEYS);
            stm.setString(1, student.getUser().getUsername());
            stm.setString(2, student.getUser().getPassword());
            stm.setInt(3, student.getUser().getIdRole());
            stm.executeUpdate();
            ResultSet rs = stm.getGeneratedKeys();
            if (rs.next()) {
                student.setId(rs.getInt(1));
            }
            stm.close();
            stm = connection.prepareStatement(INSERT_STUDENT);
            stm.setString(1, student.getFirstName());
            stm.setString(2, student.getLastName());
            stm.setString(3, student.getMiddleName());
            stm.setString(4, student.getEmail());
            stm.setString(5, student.getPhoneNumber());
            stm.setInt(6, student.getId());
            stm.executeUpdate();
            stm.close();
            connection.commit();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbStudentDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbStudentDao" + e.getMessage(), e);
        }
    }

    @Override
    public Student read(int idStudent) throws DaoException {
        Student student = null;
        User user = null;
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            PreparedStatement stm = connection.prepareStatement(SELECT_STUDENT);
            stm.setInt(1, idStudent);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                student = new Student();
                student.setFirstName(rs.getString(ColumnName.FIRST_NAME));
                student.setLastName(rs.getString(ColumnName.LAST_NAME));
                student.setMiddleName(rs.getString(ColumnName.MIDDLE_NAME));
                student.setEmail(rs.getString(ColumnName.EMAIL));
                student.setPhoneNumber(rs.getString(ColumnName.PHONE_NUMBER));
                user = new User();
                user.setIdRole(2);
                user.setUsername(rs.getString(ColumnName.USERNAME));
                student.setUser(user);
                student.setId(rs.getInt(ColumnName.ID_STUDENT));
            }
            stm.close();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbStudentDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbStudentDao" + e.getMessage(), e);
        }
        return student;
    }

    @Override
    public void update(Student student) throws DaoException {
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            connection.setAutoCommit(false);
            PreparedStatement stm = connection.prepareStatement(UPDATE_USER);
            stm.setString(1, student.getUser().getUsername());
            stm.setInt(2, student.getId());
            stm.executeUpdate();
            stm.close();
            stm = connection.prepareStatement(UPDATE_STUDENT);
            stm.setString(1, student.getFirstName());
            stm.setString(2, student.getLastName());
            stm.setString(3, student.getMiddleName());
            stm.setString(4, student.getEmail());
            stm.setString(5, student.getPhoneNumber());
            stm.setInt(6, student.getId());
            stm.executeUpdate();
            stm.close();
            connection.commit();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbStudentDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbStudentDao" + e.getMessage(), e);
        }
    }

    @Override
    public void delete(Student student) throws DaoException {
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            PreparedStatement stm = connection.prepareStatement(DELETE_STUDENT);
            stm.setInt(1, student.getId());
            stm.executeUpdate();
            stm.close();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbStudentDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbStudentDao" + e.getMessage(), e);
        }
    }

    @Override
    public List<Student> getAll() throws DaoException {
        List<Student> list = new ArrayList<Student>();
        Student student = null;
        User user = null;
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            PreparedStatement stm = connection.prepareStatement(SELECT_STUDENTS);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                student = new Student();
                student.setFirstName(rs.getString(ColumnName.FIRST_NAME));
                student.setLastName(rs.getString(ColumnName.LAST_NAME));
                student.setMiddleName(rs.getString(ColumnName.MIDDLE_NAME));
                student.setEmail(rs.getString(ColumnName.EMAIL));
                student.setPhoneNumber(rs.getString(ColumnName.PHONE_NUMBER));
                student.setId(rs.getInt(ColumnName.ID_STUDENT));
                user = new User();
                user.setIdRole(2);
                user.setUsername(rs.getString(ColumnName.USERNAME));
                student.setUser(user);
                list.add(student);
            }
            stm.close();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbStudentDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbStudentDao" + e.getMessage(), e);
        }
        return list;
    }

    @Override
    public List<Student> getAllStudentsByCourse(int idCourse) throws DaoException {
        List<Student> list = new ArrayList<Student>();
        Student student = null;
        User user = null;
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            PreparedStatement stm = connection.prepareStatement(SELECT_STUDENTS_BY_COURSE);
            stm.setInt(1, idCourse);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                student = new Student();
                student.setFirstName(rs.getString(ColumnName.FIRST_NAME));
                student.setLastName(rs.getString(ColumnName.LAST_NAME));
                student.setMiddleName(rs.getString(ColumnName.MIDDLE_NAME));
                student.setEmail(rs.getString(ColumnName.EMAIL));
                student.setPhoneNumber(rs.getString(ColumnName.PHONE_NUMBER));
                student.setId(rs.getInt(ColumnName.ID_STUDENT));
                user = new User();
                user.setIdRole(2);
                user.setUsername(rs.getString(ColumnName.USERNAME));
                student.setUser(user);
                list.add(student);
            }
            stm.close();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbStudentDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbStudentDao" + e.getMessage(), e);
        }
        return list;
    }
}
