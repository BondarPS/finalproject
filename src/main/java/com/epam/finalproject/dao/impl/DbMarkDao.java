/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.dao.impl;

import com.epam.finalproject.dao.MarkDao;
import com.epam.finalproject.dao.util.ConnectionPool;
import com.epam.finalproject.dao.util.ConnectionPoolException;
import com.epam.finalproject.model.Course;
import com.epam.finalproject.model.Mark;
import com.epam.finalproject.model.Student;
import com.epam.finalproject.model.Teacher;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Pavel
 */
public class DbMarkDao implements MarkDao {

    private final static String SELECT_MARK = "SELECT m.idMark, m.idStudent, m.idCourse, m.mark, m.comments, "
            + "c.CourseName, c.StartDate, c.EndDate, c.isEnded, t.idTeacher, t.firstName, t.lastName, t.middleName, t.email, t.phoneNumber "
            + "FROM electives.mark m "
            + "INNER JOIN electives.course c ON c.idCourse = m.idCourse INNER JOIN electives.teacher t ON t.idTEacher = c.idTeacher WHERE idMark = ?;";
    private final static String SELECT_STUDENT_MARKS = "SELECT m.idMark, m.idStudent, m.idCourse, m.mark, m.comments, "
            + "c.CourseName, c.StartDate, c.EndDate, c.isEnded, t.idTeacher, t.firstName, t.lastName, t.middleName, t.email, t.phoneNumber "
            + "FROM electives.mark m "
            + "INNER JOIN electives.course c ON c.idCourse = m.idCourse INNER JOIN electives.teacher t ON t.idTEacher = c.idTeacher where m.idStudent = ?;";
    private final static String SELECT_COURSE_MARKS = "SELECT m.idMark, m.idStudent, m.idCourse, m.mark, m.comments, "
            + "c.CourseName, c.StartDate, c.EndDate, c.isEnded, s.idStudent, s.firstName, s.lastName, s.middleName, s.email, s.phoneNumber "
            + "FROM electives.mark m INNER JOIN electives.course c ON c.idCourse = m.idCourse "
            + "INNER JOIN electives.student s ON m.idStudent = s.idStudent where m.idCourse = ?;";
    private final static String INSERT_MARK = "INSERT INTO electives.mark ( idStudent, idCourse)  VALUES (?, ?);";
    private final static String UPDATE_MARK = "UPDATE electives.mark SET "
            + "mark = ?, comments = ? WHERE idMark = ?;";


    public DbMarkDao()  {
    }

    @Override
    public void create(Mark mark) throws DaoException {
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            PreparedStatement stm = connection.prepareStatement(INSERT_MARK);
            stm.setInt(1, mark.getStudent().getId());
            stm.setInt(2, mark.getCourse().getId());
            stm.executeUpdate();
            stm.close();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbMarkDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbMarkDao" + e.getMessage(), e);
        }
    }

    @Override
    public Mark read(int idMark) throws DaoException {
        Mark mark = null;
        Course course = null;
        Teacher teacher = null;
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            PreparedStatement stm = connection.prepareStatement(SELECT_MARK);
            stm.setInt(1, idMark);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                mark = new Mark();
                mark.setId(rs.getInt(ColumnName.ID_MARK));
                mark.setMark(rs.getInt(ColumnName.MARK));
                mark.setComments(rs.getString(ColumnName.COMMENTS));
                course = new Course ();
                course.setId(rs.getInt(ColumnName.ID_COURSE));
                course.setCourseName(rs.getString(ColumnName.COURSE_NAME));
                course.setStartDate(rs.getDate(ColumnName.START_DATE));
                course.setEndDate(rs.getDate(ColumnName.END_DATE));
                course.setEnded(rs.getBoolean(ColumnName.IS_ENDED));
                teacher = new Teacher ();
                teacher.setId(rs.getInt(ColumnName.ID_TEACHER));
                teacher.setFirstName(rs.getString(ColumnName.FIRST_NAME));
                teacher.setLastName(rs.getString(ColumnName.LAST_NAME));
                teacher.setMiddleName(rs.getString(ColumnName.MIDDLE_NAME));
                teacher.setEmail(rs.getString(ColumnName.EMAIL));
                teacher.setPhoneNumber(rs.getString(ColumnName.PHONE_NUMBER));
                course.setTeacher(teacher);
                mark.setCourse(course);
            }
            stm.close();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbMarkDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbMarkDao" + e.getMessage(), e);
        }
        return mark;
    }

    @Override
    public void update(Mark mark) throws DaoException {
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            PreparedStatement stm = connection.prepareStatement(UPDATE_MARK);
            stm.setInt(1, mark.getMark());
            stm.setString(2, mark.getComments());
            stm.setInt(3, mark.getId());
            stm.executeUpdate();
            stm.close();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbMarkDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbMarkDao" + e.getMessage(), e);
        }
    }

   
    @Override
    public List<Mark> getAllStudentMarks(int idStudent) throws DaoException {
        List<Mark> list = new ArrayList<Mark>();
        Mark mark = null;
        Course course = null;
        Teacher teacher = null;
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            PreparedStatement stm = connection.prepareStatement(SELECT_STUDENT_MARKS);
            stm.setInt(1, idStudent);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                mark = new Mark();
                mark.setId(rs.getInt(ColumnName.ID_MARK));                
                mark.setMark(rs.getInt(ColumnName.MARK));
                mark.setComments(rs.getString(ColumnName.COMMENTS));
                course = new Course ();
                course.setId(rs.getInt(ColumnName.ID_COURSE));
                course.setCourseName(rs.getString(ColumnName.COURSE_NAME));
                course.setStartDate(rs.getDate(ColumnName.START_DATE));
                course.setEndDate(rs.getDate(ColumnName.END_DATE));
                course.setEnded(rs.getBoolean(ColumnName.IS_ENDED));
                teacher = new Teacher ();
                teacher.setId(rs.getInt(ColumnName.ID_TEACHER));
                teacher.setFirstName(rs.getString(ColumnName.FIRST_NAME));
                teacher.setLastName(rs.getString(ColumnName.LAST_NAME));
                teacher.setMiddleName(rs.getString(ColumnName.MIDDLE_NAME));
                teacher.setEmail(rs.getString(ColumnName.EMAIL));
                teacher.setPhoneNumber(rs.getString(ColumnName.PHONE_NUMBER));
                course.setTeacher(teacher);
                mark.setCourse(course);
                list.add(mark);
            }
            stm.close();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbMarkDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbMarkDao" + e.getMessage(), e);
        }
        return list;
    }
    
    @Override
    public List<Mark> getAllCourseMarks(int idCourses) throws DaoException {
        List<Mark> list = new ArrayList<Mark>();
        Mark mark = null;
        Student student = null;  
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            PreparedStatement stm = connection.prepareStatement(SELECT_COURSE_MARKS);
            stm.setInt(1, idCourses);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                mark = new Mark();
                mark.setId(rs.getInt(ColumnName.ID_MARK));                
                mark.setMark(rs.getInt(ColumnName.MARK));
                mark.setComments(rs.getString(ColumnName.COMMENTS));
                student = new Student ();
                student.setId(rs.getInt(ColumnName.ID_STUDENT));
                student.setFirstName(rs.getString(ColumnName.FIRST_NAME));
                student.setLastName(rs.getString(ColumnName.LAST_NAME));
                student.setMiddleName(rs.getString(ColumnName.MIDDLE_NAME));
                student.setEmail(rs.getString(ColumnName.EMAIL));
                student.setPhoneNumber(rs.getString(ColumnName.PHONE_NUMBER));                
                mark.setStudent(student);
                list.add(mark);
            }
            stm.close();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbMarkDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbMarkDao" + e.getMessage(), e);
        }
        return list;
    }
}
