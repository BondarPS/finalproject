/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.dao.impl;

import com.epam.finalproject.dao.RoleDao;
import com.epam.finalproject.dao.util.ConnectionPool;
import com.epam.finalproject.dao.util.ConnectionPoolException;
import com.epam.finalproject.model.Role;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Pavel
 */
public class DbRoleDao implements RoleDao {

    private final static String SELECT_ROLE = "SELECT idRole, name, description FROM electives.role WHERE idRole = ?;";
    private final static String SELECT_ROLES = "SELECT idRole, name, description FROM electives.role;";

    public DbRoleDao(){
    }

    @Override
    public Role read(int RoleId) throws DaoException {
        Role role = null;
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            PreparedStatement stm = connection.prepareStatement(SELECT_ROLE);
            stm.setString(1, Integer.toString(RoleId));
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                role = new Role();
                role.setId(rs.getInt(ColumnName.ID_ROLE));
                role.setName(rs.getString(ColumnName.NAME));
                role.setDescription(rs.getString(ColumnName.DESCRIPTION));
            }
            stm.close();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbRoleDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbRoleDao" + e.getMessage(), e);
        }
        return role;
    }

    @Override
    public List<Role> getAll() throws DaoException {
        List<Role> list = new ArrayList<Role>();
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            PreparedStatement stm = connection.prepareStatement(SELECT_ROLES);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Role role = new Role();
                role.setId(rs.getInt(ColumnName.ID_ROLE));
                role.setName(rs.getString(ColumnName.NAME));
                role.setDescription(rs.getString(ColumnName.DESCRIPTION));
                list.add(role);
            }
            stm.close();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbRoleDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbRoleDao" + e.getMessage(), e);
        }
        return list;
    }
}
