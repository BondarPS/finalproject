/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.dao.impl;

import com.epam.finalproject.dao.UserDao;
import com.epam.finalproject.dao.util.ConnectionPool;
import com.epam.finalproject.dao.util.ConnectionPoolException;
import com.epam.finalproject.model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Pavel
 */
public class DbUserDao implements UserDao {

    private final static String SELECT_USER_BY_USERNAME = "SELECT idUser, username, password, idRole FROM electives.user WHERE username = ?;";
    private final static String SELECT_USER_BY_ID = "SELECT idUser, username, password, idRole FROM electives.user WHERE idUser = ?;";
    private final static String SELECT_USERS = "SELECT idUser, username, password, idRole FROM electives.User;";
    private final static String INSERT_USER = "INSERT INTO electives.user (username, password, idRole)  VALUES (?, ?, ?);";
    private final static String UPDATE_USER = "UPDATE electives.user SET "
            + "username = ?, password = ?, idRole = ? WHERE idUser = ?;";
    private final static String DELETE_USER = "DELETE FROM electives.user where idUser = ?;";

    public DbUserDao(){
    }

    @Override
    public void create(User user) throws DaoException {
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            PreparedStatement stm = connection.prepareStatement(INSERT_USER);
            stm.setString(1, user.getUsername());
            stm.setString(2, user.getPassword());
            stm.setInt(3, user.getIdRole());
            stm.executeUpdate();
            stm.close();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbUserDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbUserDao" + e.getMessage(), e);
        }
    }

    @Override
    public User readByUsername(String username) throws DaoException {
        User user = null;
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            PreparedStatement stm = connection.prepareStatement(SELECT_USER_BY_USERNAME);
            stm.setString(1, username);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                user = new User();
                user.setId(rs.getInt(ColumnName.ID_USER));
                user.setUsername(rs.getString(ColumnName.USERNAME));
                user.setPassword(rs.getString(ColumnName.PASSWORD));
                user.setIdRole(rs.getInt(ColumnName.ID_ROLE));
            }
            stm.close();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbUserDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbUserDao" + e.getMessage(), e);
        }
        return user;
    }

    @Override
    public User readById(int id) throws DaoException {
        User user = null;
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            PreparedStatement stm = connection.prepareStatement(SELECT_USER_BY_ID);
            stm.setInt(1, id);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                user = new User();
                user.setId(rs.getInt(ColumnName.ID_USER));
                user.setUsername(rs.getString(ColumnName.USERNAME));
                user.setPassword(rs.getString(ColumnName.PASSWORD));
                user.setIdRole(rs.getInt(ColumnName.ID_ROLE));
            }
            stm.close();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbUserDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbUserDao" + e.getMessage(), e);
        }
        return user;
    }

    @Override
    public void update(User user) throws DaoException {
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            PreparedStatement stm = connection.prepareStatement(UPDATE_USER);
            stm.setString(1, user.getUsername());
            stm.setString(2, user.getPassword());
            stm.setInt(3, user.getIdRole());
            stm.setInt(4, user.getId());
            stm.executeUpdate();
            stm.close();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbUserDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbUserDao" + e.getMessage(), e);
        }
    }

    @Override
    public void delete(User user) throws DaoException {
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            PreparedStatement stm = connection.prepareStatement(DELETE_USER);
            stm.setInt(1, user.getId());
            stm.executeUpdate();
            stm.close();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbUserDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbUserDao" + e.getMessage(), e);
        }
    }

    @Override
    public List<User> getAll() throws DaoException {
        List<User> list = new ArrayList<User>();
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            PreparedStatement stm = connection.prepareStatement(SELECT_USERS);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt(ColumnName.ID_USER));
                user.setUsername(rs.getString(ColumnName.USERNAME));
                user.setPassword(rs.getString(ColumnName.PASSWORD));
                user.setIdRole(rs.getInt(ColumnName.ID_ROLE));
                list.add(user);
            }
            stm.close();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbUserDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbUserDao" + e.getMessage(), e);
        }
        return list;
    }
}
