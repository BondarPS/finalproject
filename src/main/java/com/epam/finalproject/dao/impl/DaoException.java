/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.dao.impl;

/**
 *
 * @author Pavel
 */
public class DaoException extends Exception{

    private static final long serialVersionUID = 1L;

    public DaoException(String msg) {
        super(msg);
    }

    public DaoException(String msg, Exception e) {
        super(msg, e);
    }
}
