/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.dao.impl;

/**
 *
 * @author Pavel
 */
public class ColumnName {

    private ColumnName() {
    }

    public static final String ID_COURSE = "idCourse";
    public static final String COURSE_NAME = "CourseName";
    public static final String START_DATE = "StartDate";
    public static final String END_DATE = "EndDate";
    public static final String ID_TEACHER = "idTeacher";
    public static final String IS_ENDED = "isEnded";

    public static final String ID_STUDENT = "idStudent";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String MIDDLE_NAME = "middleName";
    public static final String EMAIL = "email";
    public static final String PHONE_NUMBER = "phoneNumber";
    public static final String ID_MARK = "idMark";
    public static final String USERNAME = "username";
    public static final String ID_USER = "idUser";
    public static final String ID_ROLE = "idRole";
    public static final String PASSWORD = "password";
    
     public static final String MARK = "Mark";
     public static final String COMMENTS = "comments";
     public static final String NAME = "name";
     public static final String DESCRIPTION = "description";

}
