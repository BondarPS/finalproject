/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.dao.impl;

import com.epam.finalproject.dao.StudentCourseDao;
import com.epam.finalproject.dao.util.ConnectionPool;
import com.epam.finalproject.dao.util.ConnectionPoolException;
import com.epam.finalproject.model.StudentCourse;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Pavel
 */
public class DbStudentCourseDao implements StudentCourseDao {

    private final static String INSERT_STUDENT_COURSE = "INSERT INTO electives.studentcourse (idStudent, idCourse)  VALUES (?, ?);";
    private final static String DELETE_STUDENT_COURSE = "DELETE FROM electives.studentcourse where idStudentCourse = ?;";

    public DbStudentCourseDao() {
    }

    @Override
    public void create(StudentCourse studentCourse) throws DaoException {
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            PreparedStatement stm = connection.prepareStatement(INSERT_STUDENT_COURSE);
            stm.setInt(1, studentCourse.getIdStudent());
            stm.setInt(2, studentCourse.getIdCourse());
            stm.executeUpdate();
            stm.close();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbStudentCourseDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbStudentCourseDao" + e.getMessage(), e);
        }
    }

    @Override
    public void delete(StudentCourse studentCourse) throws DaoException {
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            PreparedStatement stm = connection.prepareStatement(DELETE_STUDENT_COURSE);
            stm.setInt(1, studentCourse.getId());
            stm.executeUpdate();
            stm.close();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbStudentCourseDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbStudentCourseDao" + e.getMessage(), e);
        }
    }
}
