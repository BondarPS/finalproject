/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.dao.impl;

import com.epam.finalproject.dao.CourseDao;
import com.epam.finalproject.dao.Dao;
import com.epam.finalproject.dao.MarkDao;
import com.epam.finalproject.dao.RoleDao;
import com.epam.finalproject.dao.StudentCourseDao;
import com.epam.finalproject.dao.StudentDao;
import com.epam.finalproject.dao.TeacherDao;
import com.epam.finalproject.dao.UserDao;
import com.epam.finalproject.dao.util.ConnectionPool;
import com.epam.finalproject.dao.util.ConnectionPoolException;

/**
 *
 * @author Pavel
 */
public class MysqlDao implements Dao {

    private static MysqlDao instance;

    private MysqlDao() throws DaoException {
    }

    public static MysqlDao getInstance() throws DaoException {
        if (instance == null) {
            instance = new MysqlDao();
        }
        return instance;
    }

    @Override
    public UserDao getUserDao() {
        return new DbUserDao();
    }

    @Override
    public RoleDao getRoleDao() {
        return new DbRoleDao();
    }

    @Override
    public CourseDao getCourseDao() {
        return new DbCourseDao();
    }

    @Override
    public StudentCourseDao getStudentCourseDao() {
        return new DbStudentCourseDao();
    }

    @Override
    public TeacherDao getTeacherDao() {
        return new DbTeacherDao();
    }

    @Override
    public StudentDao getStudentDao() {
        return new DbStudentDao();
    }

    @Override
    public MarkDao getMarkDao() {
        return new DbMarkDao();
    }

}
