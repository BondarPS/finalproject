/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.dao.impl;

import com.epam.finalproject.dao.TeacherDao;
import com.epam.finalproject.dao.util.ConnectionPool;
import com.epam.finalproject.dao.util.ConnectionPoolException;
import com.epam.finalproject.model.Teacher;
import com.epam.finalproject.model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Pavel
 */
public class DbTeacherDao implements TeacherDao {

    private final static String SELECT_TEACHER = "SELECT tc.idTeacher, tc.firstName, tc.lastName, tc.middleName, tc.email, tc.phoneNumber, u.username"
            + " FROM electives.teacher tc inner join electives.user u on tc.idTeacher = u.idUser WHERE tc.idTeacher = ?;";
    private final static String SELECT_TEACHERES = "SELECT tc.idTeacher, tc.firstName, tc.lastName, tc.middleName, tc.email, tc.phoneNumber, u.username "
            + "FROM electives.teacher tc inner join electives.user u on  tc.idTeacher = u.idUser;";
    private final static String INSERT_TEACHER = "INSERT INTO electives.teacher (firstName, lastName, middleName, email, phoneNumber, idTeacher)  VALUES (?, ?, ?, ?, ?, ?);";
    private final static String INSERT_USER = "INSERT INTO electives.user (username, password, idRole)  VALUES (?, ?, ?);";
    private final static String UPDATE_TEACHER = "UPDATE electives.teacher SET "
            + "firstName = ?, lastName = ?, middleName = ?, email = ?, phoneNumber = ? WHERE idTeacher = ?;";
     private final static String UPDATE_USER = "UPDATE electives.user SET "
            + "username = ? WHERE idUser = ?;";

    public DbTeacherDao(){
    }

    @Override
    public void create(Teacher teacher) throws DaoException {
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            connection.setAutoCommit(false);
            PreparedStatement stm = connection.prepareStatement(INSERT_USER, PreparedStatement.RETURN_GENERATED_KEYS);
            stm.setString(1, teacher.getUser().getUsername());
            stm.setString(2, teacher.getUser().getPassword());
            stm.setInt(3, teacher.getUser().getIdRole());
            stm.executeUpdate();
            ResultSet rs = stm.getGeneratedKeys();
            if (rs.next()) {
                teacher.setId(rs.getInt(1));
            }
            stm.close();
            stm = connection.prepareStatement(INSERT_TEACHER);
            stm.setString(1, teacher.getFirstName());
            stm.setString(2, teacher.getLastName());
            stm.setString(3, teacher.getMiddleName());
            stm.setString(4, teacher.getEmail());
            stm.setString(5, teacher.getPhoneNumber());
            stm.setInt(6, teacher.getId());
            stm.executeUpdate();
            stm.close();
            connection.commit();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbTeacherDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbTeacherDao" + e.getMessage(), e);
        }
    }

    @Override
    public Teacher read(int idTeacher) throws DaoException {
        Teacher teacher = null;
        User user = null;
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            PreparedStatement stm = connection.prepareStatement(SELECT_TEACHER);
            stm.setInt(1, idTeacher);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                teacher = new Teacher();
                teacher.setFirstName(rs.getString(ColumnName.FIRST_NAME));
                teacher.setLastName(rs.getString(ColumnName.LAST_NAME));
                teacher.setMiddleName(rs.getString(ColumnName.MIDDLE_NAME));
                teacher.setEmail(rs.getString(ColumnName.EMAIL));
                teacher.setPhoneNumber(rs.getString(ColumnName.PHONE_NUMBER));
                user = new User();
                user.setUsername(rs.getString(ColumnName.USERNAME));
                teacher.setUser(user);
                teacher.setId(idTeacher);
            }
            stm.close();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbTeacherDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbTeacherDao" + e.getMessage(), e);
        }
        return teacher;
    }

    @Override
    public void update(Teacher teacher) throws DaoException {
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            connection.setAutoCommit(false);
            PreparedStatement stm = connection.prepareStatement(UPDATE_TEACHER);
            stm.setString(1, teacher.getFirstName());
            stm.setString(2, teacher.getLastName());
            stm.setString(3, teacher.getMiddleName());
            stm.setString(4, teacher.getEmail());
            stm.setString(5, teacher.getPhoneNumber());
            stm.setInt(6, teacher.getId());
            stm.executeUpdate();
            stm.close();
            stm = connection.prepareStatement(UPDATE_USER);
            stm.setString(1, teacher.getUser().getUsername());
            stm.setInt(2, teacher.getId());
            stm.executeUpdate();
            stm.close();
            connection.commit();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbTeacherDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbTeacherDao" + e.getMessage(), e);
        }
    }

    @Override
    public List<Teacher> getAll() throws DaoException {
        List<Teacher> list = new ArrayList<Teacher>();
        Teacher teacher = null;
        User user = null;
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            PreparedStatement stm = connection.prepareStatement(SELECT_TEACHERES);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                teacher = new Teacher();
                teacher.setFirstName(rs.getString(ColumnName.FIRST_NAME));
                teacher.setLastName(rs.getString(ColumnName.LAST_NAME));
                teacher.setMiddleName(rs.getString(ColumnName.MIDDLE_NAME));
                teacher.setEmail(rs.getString(ColumnName.EMAIL));
                teacher.setPhoneNumber(rs.getString(ColumnName.PHONE_NUMBER));
                teacher.setId(rs.getInt(ColumnName.ID_TEACHER));
                user = new User();
                user.setUsername(rs.getString(ColumnName.USERNAME));
                teacher.setUser(user);
                list.add(teacher);
            }
            stm.close();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbTeacherDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbTeacherDao" + e.getMessage(), e);
        }
        return list;
    }
}
