/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.dao.impl;

import com.epam.finalproject.dao.CourseDao;
import com.epam.finalproject.dao.util.ConnectionPool;
import com.epam.finalproject.dao.util.ConnectionPoolException;
import com.epam.finalproject.model.Course;
import com.epam.finalproject.model.Teacher;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Pavel
 */
public class DbCourseDao implements CourseDao {

    private final static String SELECT_COURSE = "SELECT idCourse, CourseName, StartDate, EndDate, isEnded, idTeacher "
            + "FROM electives.course WHERE idCourse = ?;";
    private final static String SELECT_COURSE_WITH_TEACHER = "SELECT c.idCourse, c.CourseName, c.StartDate, c.EndDate, c.isEnded, c.idTeacher, "
            + "tc.firstName, tc.lastName, tc.middleName, tc.email, tc.phoneNumber FROM electives.course c "
            + "left join electives.teacher tc on c.idTeacher = tc.idTeacher WHERE c.idCourse = ?;";
    private final static String SELECT_COURSES = "SELECT idCourse, CourseName, StartDate, EndDate, isEnded, idTeacher "
            + " FROM electives.course;";
    private final static String SELECT_COURSES_BY_TEACHER = "SELECT idCourse, CourseName, StartDate, EndDate, isEnded, idTeacher "
            + " FROM electives.course WHERE idTeacher = ?;";
    private final static String SELECT_COURSES_BY_STUDENT = "SELECT c.idCourse, c.CourseName, c.StartDate, c.EndDate, c.isEnded, c.idTeacher "
            + " FROM electives.course c INNER JOIN electives.studentcourse s ON s.idCourse = c.idCourse WHERE s.idStudent = ?;";
    private final static String SELECT_NOT_STUDENT_COURSES = "SELECT c.idCourse, c.CourseName, c.StartDate, c.EndDate, c.isEnded, c.idTeacher FROM electives.course c " 
            + "where idCourse not in(SELECT c.idCourse FROM electives.course c INNER JOIN electives.studentcourse s ON s.idCourse = c.idCourse WHERE s.idStudent = ?);";
    private final static String UPDATE_COURSE = "UPDATE electives.course SET "
            + "CourseName = ?, StartDate = ?, EndDate = ?, isEnded = ?, idTeacher = ? WHERE idCourse = ?;";
    private final static String INSERT_COURSE = "INSERT INTO electives.course (CourseName, StartDate, EndDate, isEnded)  VALUES (?, ?, ?, ?);";

    public DbCourseDao() {
    }

    @Override
    public void create(Course course) throws DaoException {
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            PreparedStatement stm = connection.prepareStatement(INSERT_COURSE);
            stm.setString(1, course.getCourseName());
            stm.setDate(2, new java.sql.Date(course.getStartDate().getTime()));
            stm.setDate(3, new java.sql.Date(course.getEndDate().getTime()));
            stm.setInt(4, course.isEnded() ? 1 : 0);
            stm.executeUpdate();
            stm.close();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbCourseDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbCourseDao" + e.getMessage(), e);
        }
    }

    @Override
    public Course read(int idCourse) throws DaoException {
        Course course = null;
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            PreparedStatement stm = connection.prepareStatement(SELECT_COURSE);
            stm.setString(1, Integer.toString(idCourse));
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                course = new Course();
                course.setId(rs.getInt(ColumnName.ID_COURSE));
                course.setCourseName(rs.getString(ColumnName.COURSE_NAME));
                course.setStartDate(rs.getDate(ColumnName.START_DATE));
                course.setEndDate(rs.getDate(ColumnName.END_DATE));
                course.setEnded(rs.getBoolean(ColumnName.IS_ENDED));
            }
            stm.close();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbCourseDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbCourseDao" + e.getMessage(), e);
        }
        return course;
    }

    @Override
    public Course readWithTeacher(int idCourse) throws DaoException {
        Course course = null;
        Teacher teacher = null;
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            PreparedStatement stm = connection.prepareStatement(SELECT_COURSE_WITH_TEACHER);
            stm.setString(1, Integer.toString(idCourse));
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                course = new Course();
                course.setId(rs.getInt(ColumnName.ID_COURSE));
                course.setCourseName(rs.getString(ColumnName.COURSE_NAME));
                course.setStartDate(rs.getDate(ColumnName.START_DATE));
                course.setEndDate(rs.getDate(ColumnName.END_DATE));
                course.setEnded(rs.getBoolean(ColumnName.IS_ENDED));
                if (rs.getInt(ColumnName.ID_TEACHER) != 0) {
                    teacher = new Teacher();
                    teacher.setId(rs.getInt(ColumnName.ID_TEACHER));
                    teacher.setFirstName(rs.getString(ColumnName.FIRST_NAME));
                    teacher.setLastName(rs.getString(ColumnName.LAST_NAME));
                    teacher.setMiddleName(rs.getString(ColumnName.MIDDLE_NAME));
                    teacher.setEmail(rs.getString(ColumnName.EMAIL));
                    teacher.setPhoneNumber(rs.getString(ColumnName.PHONE_NUMBER));
                }
                course.setTeacher(teacher);
            }
            stm.close();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbCourseDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbCourseDao" + e.getMessage(), e);
        }
        return course;
    }

    @Override
    public void update(Course course) throws DaoException {
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            PreparedStatement stm = connection.prepareStatement(UPDATE_COURSE);
            stm.setString(1, course.getCourseName());
            stm.setDate(2, new java.sql.Date(course.getStartDate().getTime()));
            stm.setDate(3, new java.sql.Date(course.getEndDate().getTime()));
            stm.setInt(4, course.isEnded() ? 1 : 0);
            stm.setInt(5, course.getTeacher().getId());
            stm.setInt(6, course.getId());
            stm.executeUpdate();
            stm.close();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbCourseDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbCourseDao" + e.getMessage(), e);
        }
    }

    @Override
    public List<Course> getAll() throws DaoException {
        List<Course> list = new ArrayList<Course>();
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            PreparedStatement stm = connection.prepareStatement(SELECT_COURSES);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Course course = new Course();
                course.setId(rs.getInt(ColumnName.ID_COURSE));
                course.setCourseName(rs.getString(ColumnName.COURSE_NAME));
                course.setStartDate(rs.getDate(ColumnName.START_DATE));
                course.setEndDate(rs.getDate(ColumnName.END_DATE));
                course.setEnded(rs.getBoolean(ColumnName.IS_ENDED));
                list.add(course);
            }
            stm.close();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbCourseDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbCourseDao" + e.getMessage(), e);
        }
        return list;
    }

    @Override
    public List<Course> getAllTeacherCourses(int idTeacher) throws DaoException {
        List<Course> list = new ArrayList<Course>();
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            PreparedStatement stm = connection.prepareStatement(SELECT_COURSES_BY_TEACHER);
            stm.setInt(1, idTeacher);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Course course = new Course();
                course.setId(rs.getInt(ColumnName.ID_COURSE));
                course.setCourseName(rs.getString(ColumnName.COURSE_NAME));
                course.setStartDate(rs.getDate(ColumnName.START_DATE));
                course.setEndDate(rs.getDate(ColumnName.END_DATE));
                course.setEnded(rs.getBoolean(ColumnName.IS_ENDED));
                list.add(course);
            }
            stm.close();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbCourseDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbCourseDao" + e.getMessage(), e);
        }
        return list;
    }
    
    @Override
    public List<Course> getAllStuentCourses(int idStudent) throws DaoException {
        List<Course> list = new ArrayList<Course>();
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            PreparedStatement stm = connection.prepareStatement(SELECT_COURSES_BY_STUDENT);
            stm.setInt(1, idStudent);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Course course = new Course();
                course.setId(rs.getInt(ColumnName.ID_COURSE));
                course.setCourseName(rs.getString(ColumnName.COURSE_NAME));
                course.setStartDate(rs.getDate(ColumnName.START_DATE));
                course.setEndDate(rs.getDate(ColumnName.END_DATE));
                course.setEnded(rs.getBoolean(ColumnName.IS_ENDED));
                list.add(course);
            }
            stm.close();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbCourseDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbCourseDao" + e.getMessage(), e);
        }
        return list;
    }
    
    @Override
    public List<Course> getAllNotStuentCourses(int idStudent) throws DaoException {
        List<Course> list = new ArrayList<Course>();
        try {
            Connection connection = ConnectionPool.getInstance().retrieve();
            PreparedStatement stm = connection.prepareStatement(SELECT_NOT_STUDENT_COURSES);
            stm.setInt(1, idStudent);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Course course = new Course();
                course.setId(rs.getInt(ColumnName.ID_COURSE));
                course.setCourseName(rs.getString(ColumnName.COURSE_NAME));
                course.setStartDate(rs.getDate(ColumnName.START_DATE));
                course.setEndDate(rs.getDate(ColumnName.END_DATE));
                course.setEnded(rs.getBoolean(ColumnName.IS_ENDED));
                list.add(course);
            }
            stm.close();
            ConnectionPool.getInstance().putback(connection);
        } catch (SQLException e) {
            throw new DaoException("SqlException generated in DbCourseDao" + e.getMessage(), e);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException generated in DbCourseDao" + e.getMessage(), e);
        }
        return list;
    }
}
