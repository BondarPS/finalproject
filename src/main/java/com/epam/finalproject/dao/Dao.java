/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epam.finalproject.dao;

import com.epam.finalproject.dao.impl.DaoException;

/**
 *
 * @author Pavel
 */
public interface Dao {

    public UserDao getUserDao();

    public RoleDao getRoleDao();

    public CourseDao getCourseDao();

    public TeacherDao getTeacherDao();
    
    public StudentDao getStudentDao();
    
    public MarkDao getMarkDao();

    public StudentCourseDao getStudentCourseDao();
}
